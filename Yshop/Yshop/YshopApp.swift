//
//  YshopApp.swift
//  Yshop
//
//  Created by GST.SYN.APL on 13/07/2023.
//

import SwiftUI
import Firebase
import GoogleSignIn
import FirebaseCore

@main
struct YshopApp: App {
    @AppStorage(Localization.isFirstRun) private var isFirstRun: Bool?
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate

    init() {
        setupAuthentication()
    }

    var body: some Scene {
        WindowGroup {
            if isFirstRun == nil {
                SplashView()
                    .preferredColorScheme(.light)
            } else if isFirstRun == false {
                AppView()
                    .environmentObject(ProductInApp())
                    .environmentObject(CategoryModel())
                    .preferredColorScheme(.light)
            } else {
                LoginView()
                    .preferredColorScheme(.light)
                    .onOpenURL { url in
                        GIDSignIn.sharedInstance.handle(url)
                    }
            }
        }
    }


}

extension YshopApp {
    private func setupAuthentication() {
        FirebaseApp.configure()
    }
}
