//
//  UserViewModel.swift
//  Yshop
//
//  Created by GST.SYN.APL on 07/08/2023.
//

import Firebase
import FirebaseStorage
import SwiftUI

class UserViewModel: ObservableObject {
    @Published var email: String = ""
    @Published var fullName: String = ""
    @Published var urlImage: URL?
    @Published var password: String = ""
    @Published var image: UIImage?
    @Published var isShowPopupError: Bool = false
    @Published var isSuccess: Bool = false

    private var oldFullName: String = ""
    private var oldPassword: String = ""

    private var storage = Storage.storage()
    let ref = Database.database().reference()

    func getProfile() {
        let user = Auth.auth().currentUser
        if let user = user {
            email = user.email ?? ""
            urlImage = user.photoURL
            fullName = user.displayName ?? ""
            oldFullName = fullName
            ref.child(user.uid).getData(completion:  { error, snapshot in
                guard error == nil else {
                    return
                }
                let userName = snapshot?.value as? NSDictionary
                guard let data = userName else {
                    return
                }
                
                self.password = data.value(forKey: "password") as? String ?? ""
                self.oldPassword = self.password
            })
        }
    }

    func uploadImageAndUpdateProfile() {
        if let imageData = image?.jpegData(compressionQuality: 0.8) {
            let storageRef = storage.reference().child("profile_images").child("\(UUID().uuidString).jpg")

            storageRef.putData(imageData, metadata: nil) { metadata, error in
                if let error = error {
                    print("Error uploading image: \(error.localizedDescription)")
                } else {
                    storageRef.downloadURL { url, error in
                        if let downloadURL = url {
                            self.updateProfileWithImageURL(downloadURL.absoluteString)
                            self.urlImage = url
                        } else if let error = error {
                            print("Error getting download URL: \(error.localizedDescription)")
                        }
                    }
                }
            }
        }
    }

    private func updateProfileWithImageURL(_ imageURL: String) {
        if let user = Auth.auth().currentUser {
            let changeRequest = user.createProfileChangeRequest()
            changeRequest.photoURL = URL(string: imageURL)

            changeRequest.commitChanges { error in
                if let error = error {
                    print("Error updating profile: \(error.localizedDescription)")
                } else {
                    // Profile updated successfully
                    print("Profile updated successfully!")
                }
            }
        }
    }

    func updateProfile(isUpdatePass: Bool) {
        if oldFullName.elementsEqual(fullName) && oldPassword.elementsEqual(password) {
            return
        }

        if !password.isPasswordValid() || fullName == "" {
            isShowPopupError = true
        } else {
            if let user = Auth.auth().currentUser {
                let changeRequest = user.createProfileChangeRequest()
                changeRequest.displayName = fullName

                changeRequest.commitChanges { error in
                    if let error = error {
                        print("Error updating profile: \(error.localizedDescription)")
                    } else {
                        self.isSuccess = true
                        print("Profile updated successfully!")
                    }
                }

                if isUpdatePass {
                    user.updatePassword(to: password)
                    ref.child(user.uid).child("password").setValue(password)
                }
            }
        }
    }
}
