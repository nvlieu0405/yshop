//
//  UserView.swift
//  Yshop
//
//  Created by GST.SYN.APL on 14/07/2023.
//

import SwiftUI
import PhotosUI
import Firebase
import Kingfisher
import GoogleSignIn

struct UserView: View {
    @State var showAction: Bool = false
    @State var showImagePicker: Bool = false
    @State var uiImage: UIImage? = nil
    @State var isEditting: Bool = false
    @State var isLogoutSuccess: Bool = false
    @State var isSecurePass: Bool = true
    @AppStorage("isLoginGoogle") private var isLoginGoogle: Bool?
    @ObservedObject private var model = UserViewModel()
    let firebaseAuth = Auth.auth()

    var body: some View {
        if firebaseAuth.currentUser == nil {
            VStack {
                Spacer()
                Button {
                    isLogoutSuccess = true
                } label: {
                    ZStack {
                        Capsule()
                            .fill(Color(NameConstant.Color.Color5B9EE1))
                            .frame(height: 52)
                        Text(Localization.signIn)
                            .font(
                                Font.customWMd(size: 18)
                                    .weight(.medium)
                            )
                            .foregroundColor(.white)
                    }
                }
                .padding(.horizontal, 35)
                
            }
            .background(
                Color(NameConstant.Color.ColorF8F9FA)
            )
            .buttonStyle(PlainButtonStyle())
            .fullScreenCover(isPresented: $isLogoutSuccess) {
                LoginView()
                    .preferredColorScheme(.light)
                    .onOpenURL { url in
                        GIDSignIn.sharedInstance.handle(url)
                    }
            }
        } else {
            GeometryReader { geo in
                VStack {
                    HStack(alignment: .center) {
                        Spacer()

                        Text(Localization.profile)
                            .font(
                                Font.customWMd(size: 16)
                                    .weight(.medium)
                            )
                            .foregroundColor(
                                Color(NameConstant.Color.Color1A2530)
                            )
                            .padding(.leading, 60)

                        Spacer()

                        Button {
                            if isEditting {
                                model.updateProfile(isUpdatePass: isLoginGoogle == false)
                            }
                            isEditting.toggle()
                        } label: {
                            if !isEditting {
                                Image(NameConstant.Image.editIconBlue)
                                    .frame(width: 24, height: 24)
                                    .padding(.trailing, 35)
                            } else {
                                Image(systemName: "checkmark")
                                    .foregroundColor(.blue)
                                    .frame(width: 24, height: 24)
                                    .padding(.trailing, 35)
                            }
                        }

                    }
                    .padding(.top, 10)

                    ZStack {
                        KFImage(model.urlImage)
                            .placeholder({
                                Image(systemName: "person")
                                    .resizable()
                                    .foregroundColor(.gray)
                                    .frame(width: 20,height: 20)
                                if model.urlImage != nil {
                                    ProgressView()
                                        .progressViewStyle(CircularProgressViewStyle())
                                }
                            })
                            .resizable()
                            .frame(width: 90, height: 90)
                            .background(
                                Color(NameConstant.Color.Color5B9EE1).opacity(0.3)
                            )
                            .clipShape(Circle())

                        Image("camera")
                            .resizable()
                            .frame(width: 16, height: 16)
                            .foregroundColor(.white)
                            .background(
                                Circle()
                                    .fill(
                                        Color(NameConstant.Color.Color5B9EE1)
                                    )
                                    .frame(width: 30, height: 30)
                            )
                            .offset(y: 40)
                            .onTapGesture {
                                self.showImagePicker = true
                            }
                    }
                    .sheet(isPresented: $showImagePicker, onDismiss: {
                        self.showImagePicker = false
                    }, content: {
                        ImagePickerView(sourceType: .photoLibrary) { image in
                            model.image = image
                            model.uploadImageAndUpdateProfile()
                        }
                    })
                    .padding(.top, 10)

                    Text(model.fullName)
                        .font(
                            Font.customWMd(size: 20)
                                .weight(.medium)
                        )
                        .multilineTextAlignment(.center)
                        .foregroundColor(
                            Color(NameConstant.Color.Color1A2530)
                        )

                    VStack(alignment: .leading, spacing: 0) {
                        Text(Localization.fullName)
                            .font(
                                Font.customWMd(size: 16)
                                    .weight(.medium)
                            )
                            .foregroundColor(
                                Color(NameConstant.Color.Color1A2530)
                            )
                        HStack {
                            TextField(Localization.fullName, text: $model.fullName)
                                .disabled(!isEditting)
                        }
                        .modifier(TextFieldModifier())
                        .padding(.top, 12)

                        Text(Localization.emailAddress)
                            .font(
                                Font.customWMd(size: 16)
                                    .weight(.medium)
                            )
                            .foregroundColor(
                                Color(NameConstant.Color.Color1A2530)
                            )
                            .padding(.top, 16)

                        HStack {
                            TextField(Localization.emailAddress, text: $model.email)
                                .disabled(true)
                        }
                        .modifier(TextFieldModifier(isDisable: true))
                        .padding(.top, 12)

                        if isLoginGoogle == false {
                            Text(Localization.password)
                                .font(
                                    Font.customWMd(size: 16)
                                        .weight(.medium)
                                )
                                .foregroundColor(
                                    Color(NameConstant.Color.Color1A2530)
                                )
                                .padding(.top, 16)

                            HStack {
                                if isEditting {
                                    if isSecurePass {
                                        SecureField(Localization.password, text: $model.password)
                                            .disabled(!isEditting)
                                    } else {
                                        TextField(Localization.password, text: $model.password)
                                    }
                                    Button {
                                        isSecurePass.toggle()
                                    } label: {
                                        Image(systemName: isSecurePass ? "eye.slash" : "eye")
                                            .padding()
                                    }
                                } else {
                                    SecureField(Localization.password, text: $model.password)
                                        .disabled(!isEditting)
                                }
                            }
                            .modifier(TextFieldModifier())
                            .padding(.top, 12)
                        } else {
                            Spacer()
                        }
                    }
                    .padding(.horizontal, 20)
                    .padding(.top, 32)

                    Button {
                        do {
                            try firebaseAuth.signOut()
                            isLogoutSuccess = true
                            isLoginGoogle = false
                        } catch let signOutError as NSError {
                            print("Error signing out: %@", signOutError)
                        }
                    } label: {
                        ZStack {
                            Capsule()
                                .fill(Color(NameConstant.Color.Color5B9EE1))
                                .frame(height: 52)
                            Text(Localization.signOut)
                                .font(
                                    Font.customWMd(size: 18)
                                        .weight(.medium)
                                )
                                .foregroundColor(.white)
                        }
                    }
                    .padding(.top, 100)
                    .padding(.horizontal, 35)

                    Spacer()

                }
                .alert(Text(Localization.success),
                       isPresented: $model.isSuccess,
                       actions: {
                    VStack {
                        Text(Localization.ok)
                    }
                }, message: {
                    VStack {
                        Text(Localization.updateProfileSuccess)
                        Text(Localization.updateProfileSuccess)
                    }
                })
                .alert(Text(Localization.error),
                       isPresented: $model.isShowPopupError,
                       actions: {
                    VStack {
                        Text(Localization.ok)
                    }
                }, message: {
                    VStack {
                        Text(Localization.errorInfor)
                    }
                })
                .onAppear {
                    model.getProfile()
                }
                .padding(.bottom, geo.safeAreaInsets.bottom)
                .background(
                    Color(NameConstant.Color.ColorF8F9FA)
                )
                .buttonStyle(PlainButtonStyle())
                .fullScreenCover(isPresented: $isLogoutSuccess) {
                    LoginView()
                        .preferredColorScheme(.light)
                        .onOpenURL { url in
                            GIDSignIn.sharedInstance.handle(url)
                        }
                }
            }
        }
    }
}

struct UserView_Previews: PreviewProvider {
    static var previews: some View {
        UserView()
    }
}
