//
//  LoginWelcomeView.swift
//  Yshop
//
//  Created by GST.SYN.APL on 20/07/2023.
//

import SwiftUI

struct LoginWelcomeView: View {
    var title: String = ""
    var description: String = ""
    var fontSizeTitle: CGFloat = 28
    var body: some View {
        VStack {
            Text(title)
                .font(
                    Font.customWMd(size: fontSizeTitle)
                        .weight(.medium)
                )
                .multilineTextAlignment(.center)
                .foregroundColor(
                    Color(NameConstant.Color.Color1A2530)
                )
            Text(description)
                .font(
                    Font.customWMd(size: 16)
                )
                .multilineTextAlignment(.center)
                .foregroundColor(
                    Color(NameConstant.Color.Color707B81)
                )
        }
    }
}

struct LoginWelcomeView_Previews: PreviewProvider {
    static var previews: some View {
        LoginWelcomeView()
    }
}
