//
//  ForgotPasswordView.swift
//  Yshop
//
//  Created by GST.SYN.APL on 20/07/2023.
//

import SwiftUI

struct ForgotPasswordView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @State private var emailText: String = ""
    @State private var isEmailSuccess: Bool = false
    @State private var isEmailFail: Bool = false

    var body: some View {
        VStack {
            HStack {
                Button {
                    presentationMode.wrappedValue.dismiss()
                    isEmailSuccess = emailText.isValidEmail()
                    isEmailFail = !emailText.isValidEmail()
                } label: {
                    Image(systemName: "chevron.backward")
                        .myImageModifier()
                }

                Spacer()
            }
            .padding(.top, 18)

            LoginWelcomeView(title: Localization.recoveryPassword,
                             description: Localization.resetPass,
                             fontSizeTitle: 24)
            .padding(.horizontal, 50)
            .padding(.top, 42)

            VStack(alignment: .leading, spacing: 0) {
                Text(Localization.emailAddress)
                    .font(
                        Font.customWMd(size: 16)
                            .weight(.medium)
                    )
                    .foregroundColor(
                        Color(NameConstant.Color.Color1A2530)
                    )

                HStack {
                    TextField(Localization.emailAddress, text: $emailText)
                }
                .modifier(TextFieldModifier())
                .padding(.top, 12)

                ZStack {
                    Capsule()
                        .fill(Color(NameConstant.Color.Color5B9EE1))
                        .frame(height: 52)
                    Text(Localization.continueText)
                        .font(
                            Font.customWMd(size: 18)
                                .weight(.medium)
                        )
                        .foregroundColor(.white)
                }
                .padding(.top, 40)
            }
            .padding(.top, 30)
            Spacer()
        }
        .alert(Text(Localization.yourRequestSuccess),
               isPresented: $isEmailSuccess,
               actions: {
            Text(Localization.ok)
        },
               message: {
            Text(Localization.pleaseCheckEmail)
        })
        .alert(Text(Localization.error),
               isPresented: $isEmailFail,
               actions: {
            Text(Localization.ok)
        }, message: {
            Text(Localization.pleaseEnterEmail)
        })
        .padding(.horizontal, 20)
        .background(Color(NameConstant.Color.ColorF8F9FA))
        .navigationBarBackButtonHidden(true)
    }
}

struct ForgotPasswordView_Previews: PreviewProvider {
    static var previews: some View {
        ForgotPasswordView()
    }
}
