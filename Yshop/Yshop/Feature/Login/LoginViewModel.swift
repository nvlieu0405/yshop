//
//  LoginViewModel.swift
//  Yshop
//
//  Created by GST.SYN.APL on 31/07/2023.
//

import Foundation
import Firebase
import GoogleSignIn

class LoginViewModel: ObservableObject {
    @Published var emailText: String = ""
    @Published var yourNameText: String = ""
    @Published var passwordText: String = ""
    @Published var isSecurePass: Bool = true
    @Published var showAlert: Bool = false
    @Published var isLoginSuccess: Bool = false
    @Published var alertMessage = ""
    @Published var isLoading: Bool = false

    let ref = Database.database().reference()

    func login() {
        Auth.auth().signIn(withEmail: emailText, password: passwordText) { authResult, error in
            self.isLoading = false
            if let error = error {
                self.alertMessage = error.localizedDescription
                self.showAlert = true
            } else {
                self.isLoginSuccess = true
            }
        }
    }

    func signUp() {
        if passwordText.isEmpty {
            alertMessage = Localization.passwordInvalid
            showAlert = true
            isLoading = false
            return
        }

        Auth.auth().createUser(withEmail: emailText, password: passwordText) { authResult, error in
            if let error = error {
                self.alertMessage = error.localizedDescription
                self.showAlert = true
            } else {
                let changeRequest = Auth.auth().currentUser?.createProfileChangeRequest()
                changeRequest?.displayName = self.yourNameText
                changeRequest?.commitChanges { error in
                }

                guard let uid = Auth.auth().currentUser?.uid else {
                    return
                }

                self.ref.child("\(uid)").child("password").setValue(self.passwordText)

            }
            self.isLoading = false
            self.isLoginSuccess = true
        }
    }
    
}
