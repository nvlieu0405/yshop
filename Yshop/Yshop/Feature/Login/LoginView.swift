//
//  LoginView.swift
//  Yshop
//
//  Created by GST.SYN.APL on 19/07/2023.
//

import SwiftUI
import GoogleSignIn
import Firebase
import GoogleSignInSwift

struct LoginView: View {
    @State var isLogin: Bool = true
    @ObservedObject private var model = LoginViewModel()
    @AppStorage("isLoginGoogle") private var isLoginGoogle: Bool = false
    @AppStorage("isFirstRun") private var isFirstRun: Bool?
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    var body: some View {
        NavigationView {
            ScrollView(.vertical, showsIndicators: false) {
                VStack {
                    VStack {
                        if !isLogin {
                            HStack (alignment: .center, spacing: 0) {
                                Button {
                                    isLogin = true
                                    presentationMode.wrappedValue.dismiss()
                                } label: {
                                    Image(systemName: "chevron.backward")
                                        .myImageModifier()
                                        .padding(.top, 30)
                                }

                                Spacer()
                            }
                            .padding(.horizontal, 20)
                        }
                    }

                    LoginWelcomeView(title: isLogin ? Localization.helloAgain : Localization.createAccount,
                                     description: isLogin ? Localization.welcomeBack : Localization.letCreate)
                    .padding(.top, isLogin ? 84 : 32)

                    VStack(alignment: .leading, spacing: 0) {
                        if !isLogin {
                            Text(Localization.yourName)
                                .font(
                                    Font.customWMd(size: 16)
                                        .weight(.medium)
                                )
                                .foregroundColor(
                                    Color(NameConstant.Color.Color1A2530)
                                )

                            HStack {
                                TextField(Localization.yourName, text: $model.yourNameText)
                            }
                            .modifier(TextFieldModifier())
                            .padding(.top, 12)
                            .padding(.bottom, 30)
                        }

                        Text(Localization.emailAddress)
                            .font(
                                Font.customWMd(size: 16)
                                    .weight(.medium)
                            )
                            .foregroundColor(
                                Color(NameConstant.Color.Color1A2530)
                            )

                        HStack {
                            TextField(Localization.emailAddress, text: $model.emailText)
                        }
                        .modifier(TextFieldModifier())
                        .padding(.top, 12)

                        Text(Localization.password)
                            .font(
                                Font.customWMd(size: 16)
                                    .weight(.medium)
                            )
                            .foregroundColor(
                                Color(NameConstant.Color.Color1A2530)
                            )
                            .padding(.top, 20)

                        HStack {
                            if model.isSecurePass {
                                SecureField(Localization.password, text: $model.passwordText)
                            } else {
                                TextField(Localization.password, text: $model.passwordText)
                            }
                            Button {
                                model.isSecurePass.toggle()
                            } label: {
                                Image(systemName: model.isSecurePass ? "eye.slash" : "eye")
                                    .padding()
                            }
                        }
                        .modifier(TextFieldModifier())
                        .padding(.top ,12)
                    }
                    .padding(.horizontal, 20)
                    .padding(.top, 50)

                    if isLogin {
                        NavigationLink(destination:
                                        ForgotPasswordView()
                                       ,
                                       label: {
                            HStack {
                                Spacer()
                                Text(Localization.recoveryPassword)
                                    .font(Font.customWMd(size: 13))
                                    .frame(height: 40,alignment: .trailing)
                                    .multilineTextAlignment(.trailing)
                                    .foregroundColor(
                                        Color(NameConstant.Color.Color707B81)
                                    )
                            }
                            .padding(.horizontal, 20)
                        })
                        .buttonStyle(PlainButtonStyle())
                    }

                    VStack(alignment: .center, spacing: 30) {
                        Button {
                            model.isLoading = true
                            if isLogin {
                                model.login()
                            } else {
                                model.signUp()
                            }
                        } label: {
                            ZStack {
                                Capsule()
                                    .fill(Color(NameConstant.Color.Color5B9EE1))
                                    .frame(height: 52)
                                Text(Localization.signIn)
                                    .font(
                                        Font.customWMd(size: 18)
                                            .weight(.medium)
                                    )
                                    .foregroundColor(.white)
                            }
                        }

                        Button {
                            handleSignInGoogle()
                        } label: {
                            ZStack {
                                Capsule()
                                    .fill(.white)
                                    .frame(height: 52)
                                HStack(alignment: .center, spacing: 8) {
                                    Image(NameConstant.Image.google)
                                        .resizable()
                                        .frame(width: 24, height: 24)

                                    Text(Localization.signInGG)
                                        .font(
                                            Font.customWMd(size: 18)
                                                .weight(.medium)
                                        )
                                        .foregroundColor(
                                            Color(NameConstant.Color.Color1A2530)
                                        )
                                }
                            }
                        }
                    }
                    .padding(.horizontal, 20)
                    .padding(.top, isLogin ? 0 : 30)

                    Spacer()

                    HStack(alignment: .center, spacing: 2) {
                        Text(isLogin ? Localization.dontHaveAccount : Localization.alreadyAccount)
                            .font(
                                Font.customWMd(size: 12)
                            )
                            .foregroundColor(
                                Color(NameConstant.Color.Color707B81)
                            )
                            .padding(.top, 30)

                        if isLogin {
                            NavigationLink {
                                LoginView(isLogin: false)
                            } label: {
                                Text(isLogin ? Localization.signUpFree : Localization.signIn)
                                    .font(
                                        Font.customWMd(size: 12)
                                            .weight(.medium)
                                    )
                                    .foregroundColor(
                                        Color(NameConstant.Color.Color1A2530)
                                    )
                                    .padding(.top, 30)
                            }
                        } else {
                            Button {
                                presentationMode.wrappedValue.dismiss()
                            } label: {
                                Text(isLogin ? Localization.signUpFree : Localization.signIn)
                                    .font(
                                        Font.customWMd(size: 12)
                                            .weight(.medium)
                                    )
                                    .foregroundColor(
                                        Color(NameConstant.Color.Color1A2530)
                                    )
                                    .padding(.top, 30)
                            }
                        }
                    }
                }
            }
            .background(Color(NameConstant.Color.ColorF8F9FA))
        }
        .overlay(content: {
            VStack {
                if model.isLoading {
                    ProgressView(Localization.loading)
                        .progressViewStyle(CircularProgressViewStyle())
                }
            }
            .frame(width: 200, height: 100)
            .background(model.isLoading ? .gray.opacity(0.3) : .clear)
            .cornerRadius(24)
        })
        .onAppear {
            isFirstRun = true
        }
        .onDisappear {
            isFirstRun = false
        }
        .buttonStyle(PlainButtonStyle())
        .navigationBarBackButtonHidden(true)
        .alert(isPresented: $model.showAlert) {
            Alert(title: Text(Localization.error), message: Text(model.alertMessage), dismissButton: .default(Text(Localization.ok)))
        }
        .fullScreenCover(isPresented: $model.isLoginSuccess) {
            AppView()
                .environmentObject(ProductInApp())
                .environmentObject(CategoryModel())
                .preferredColorScheme(.light)
        }
    }

    func handleSignInGoogle() {
        if let root = UIApplication.shared.windows.first?.rootViewController {
            GIDSignIn.sharedInstance.signIn(withPresenting: root) { result, error in
                guard result != nil else {
                    return
                }
                guard let user = result?.user,
                      let idToken = user.idToken?.tokenString
                else {
                    return
                }
                let credential = GoogleAuthProvider.credential(withIDToken: idToken,
                                                               accessToken: user.accessToken.tokenString)
                Auth.auth().signIn(with: credential) { result, error in
                    self.model.isLoginSuccess = true
                    self.isLoginGoogle = true
                }
            }
        }
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
