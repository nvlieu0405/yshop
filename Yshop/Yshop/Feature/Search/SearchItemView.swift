//
//  SearchItemView.swift
//  Yshop
//
//  Created by GST.SYN.APL on 07/08/2023.
//

import SwiftUI
import Kingfisher

struct SearchItemView: View {
    @State var product: ProductElement
    let width = UIScreen.main.bounds.width
    var body: some View {
        VStack {
            VStack {
                ZStack {
                    VStack {
                        KFImage(URL(string: product.gallery[0]))
                            .placeholder({ _ in
                                ProgressView()
                                    .progressViewStyle(CircularProgressViewStyle())
                            })
                            .resizable()
                            .scaledToFit()
                            .frame(maxWidth: 128)
                            .rotationEffect(Angle(degrees: -13.75))

                        Spacer()
                    }
                }

                HStack {
                    VStack(alignment: .leading) {
                        Text(Localization.bestSeller)
                            .font(Font.customWMd(size: 12))
                            .foregroundColor(
                                Color(NameConstant.Color.Color5B9EE1)
                            )

                        Text(product.name)
                            .font(
                                Font.customWMd(size: 16)
                                    .weight(.medium)
                            )
                            .foregroundColor(
                                Color(NameConstant.Color.Color1A2530)
                            )

                        HStack {
                            Text(product.price.convertPrice())
                                .font(
                                    Font.customXBd(size: 16)
                                        .weight(.medium)
                                )
                                .foregroundColor(
                                    Color(NameConstant.Color.Color1A2530)
                                )

                            Spacer()

                            HStack {
                                ForEach(product.color, id: \.self) { item in
                                    Circle()
                                        .stroke(Color.gray.opacity(0.5), lineWidth: 1)
                                        .frame(width: 16)
                                        .overlay {
                                            Circle()
                                                .fill(item.colorProduct())
                                                .frame(width: 15)
                                        }

                                }
                            }
                        }
                    }

                    Spacer()
                }
            }
            .padding(.horizontal, 14)
            .padding(.top, 20)
            .padding(.bottom, 14)
        }
        .background(.white)
        .frame(maxWidth: width - 70, maxHeight: 214)
        .cornerRadius(16)
    }
}
