//
//  SearchView.swift
//  Yshop
//
//  Created by GST.SYN.APL on 04/08/2023.
//

import SwiftUI

struct SearchView: View {
    let width = UIScreen.main.bounds.width
    @State var searchText: String = ""
    @Environment(\.presentationMode) private var presentationMode
    @EnvironmentObject private var list: ProductInApp
    @FocusState private var focusedField: Bool?
    let gridItem: [GridItem] = [.init(.flexible()), .init(.flexible())]

    func getListFilter() -> [ProductElement] {
        let listFilter: [ProductElement] = list.productList.filter {
            $0.name.contains(searchText)
        }
        return listFilter
    }

    var body: some View {
        VStack {
            HStack {
                Image(NameConstant.Image.searchIcon)
                    .resizable()
                    .frame(width: 18, height: 18)
                    .padding()

                TextField(Localization.searchText, text: $searchText)
                    .padding(.leading, 0)
                    .focused($focusedField, equals: true)


                Text(Localization.cancel)
                    .foregroundColor(
                        Color(NameConstant.Color.ColorF87265).opacity(0.9)
                    )
                    .onTapGesture {
                        presentationMode.wrappedValue.dismiss()
                    }
                    .padding(.trailing, 20)
            }
            .padding(.vertical, 27)
            .frame(width: width - 70, height: 50)
            .background(
                Capsule()
                    .fill(Color(NameConstant.Color.ColorEEEEEE))
            )

            if !getListFilter().isEmpty {
                ScrollView(.vertical, showsIndicators: false) {
                    LazyVGrid(columns: gridItem, spacing: 20) {
                        ForEach(0..<getListFilter().count, id: \.self) { index in
                            NavigationLink {
                                DetailView(product: getListFilter()[index])
                            } label: {
                                FavoriteItemView(product: getListFilter()[index], isFavoriteView: false, model: FavoriteViewModel())
                            }
                        }
                    }
                }
                .padding(.top, 24)            }

            Spacer()
        }
        .frame(maxWidth: .infinity)
        .navigationBarBackButtonHidden(true)
        .onAppear {
            focusedField = true
        }
        .background(
            Color(NameConstant.Color.ColorF8F9FA)
        )
    }
}

struct SearchView_Previews: PreviewProvider {
    static var previews: some View {
        SearchView()
            .environmentObject(ProductInApp())
    }
}
