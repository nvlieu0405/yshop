//
//  BestSellerView.swift
//  Yshop
//
//  Created by GST.SYN.APL on 26/07/2023.
//

import SwiftUI

struct BestSellerView: View {
    let gridItem: [GridItem] = [.init(.flexible()), .init(.flexible())]
    @State var productList: [ProductElement]

    var body: some View {
        VStack {
            BestSellerHeaderView()
                .padding(.top, 8)
            Spacer()

            ScrollView(.vertical, showsIndicators: false) {
                LazyVGrid(columns: gridItem, spacing: 20) {
                    ForEach(0..<productList.count, id: \.self) { index in
                        NavigationLink {
                            DetailView(product: productList[index])
                        } label: {
                            FavoriteItemView(product: productList[index],
                                             isFavoriteView: false,
                                             model: FavoriteViewModel())
                        }
                    }
                }
            }
            .padding(.top, 24)

        }
        .background(
            Color(NameConstant.Color.ColorF8F9FA)
        )
        .navigationBarBackButtonHidden(true)
    }
}
