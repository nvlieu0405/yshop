//
//  BestSellerHeaderView.swift
//  Yshop
//
//  Created by GST.SYN.APL on 03/08/2023.
//

import SwiftUI

struct BestSellerHeaderView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    var body: some View {
        HStack {
            Button {
                presentationMode.wrappedValue.dismiss()
            } label: {
                Image(systemName: "chevron.backward")
                    .myImageModifier()
            }

            Spacer()

            Text(Localization.bestSeller)
            .font(
            Font.customWMd(size: 16)
            .weight(.medium)
            )
            .foregroundColor(
                Color(NameConstant.Color.Color1A2530)
            )

            Spacer()

//            Image(systemName: NameConstant.Image.tabbarFavorite)
//                .myImageModifier()

        }
        .padding(.horizontal, 35)
    }
}

struct BestSellerHeaderView_Previews: PreviewProvider {
    static var previews: some View {
        BestSellerHeaderView()
    }
}
