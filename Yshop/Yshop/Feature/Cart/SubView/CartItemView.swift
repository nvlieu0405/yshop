//
//  CartItemView.swift
//  Yshop
//
//  Created by GST.SYN.APL on 25/07/2023.
//

import SwiftUI
import Kingfisher

struct CartItemView: View {
    @State var cart: CartModel
    @ObservedObject var model: CartViewModel
    @State private var isDelete: Bool = false
    var body: some View {
        HStack {
            KFImage(URL(string: cart.image))
                .placeholder({
                    Image(systemName: "shoeprints.fill")
                        .foregroundColor(.gray)
                })
                .resizable()
                .scaledToFit()
                .frame(width: 60)
                .shadow(color: Color(NameConstant.Color.Color707B81).opacity(0.1), radius: 6, x: 0, y: 0)
                .background {
                    Rectangle()
                        .fill(.white)
                        .frame(width: 85, height: 85)
                        .cornerRadius(24)
                }

            VStack(alignment: .leading) {
                Text(cart.name)
                    .font(
                        Font.customWMd(size: 16)
                            .weight(.medium)
                    )
                    .foregroundColor(
                        Color(NameConstant.Color.Color1A2530)
                    )

                Text(cart.price.convertPrice())
                    .font(
                        Font.customWMd(size: 14)
                            .weight(.medium)
                    )
                    .foregroundColor(
                        Color(NameConstant.Color.Color1A2530)
                    )

                HStack(alignment: .center, spacing: 16) {
                    Button {
                        model.onTapMinus(cart: cart)
                    } label: {
                        Image(NameConstant.Image.minusIcon)
                            .resizable()
                            .frame(width: 24, height: 24)
                    }

                    Text("\(cart.count)")
                        .font(Font.customWMd(size: 14))
                        .foregroundColor(Color(NameConstant.Color.Color101817))

                    Button {
                        model.onTapPlus(cart: cart)
                    } label: {
                        Image(NameConstant.Image.plusIcon)
                            .resizable()
                            .frame(width: 24, height: 24)
                    }
                }
            }
            .padding(.leading, 12)

            Spacer()

            VStack {
                Text("\(cart.size)")
                    .font(
                        Font.customWMd(size: 14)
                            .weight(.medium)
                    )
                    .foregroundColor(
                        Color(NameConstant.Color.Color1A2530)
                    )

                Spacer()

                Button {
                    isDelete = true
                } label: {
                    Image(NameConstant.Image.deleteIcon)
                        .resizable()
                        .frame(width: 24, height:  24)
                }
            }
            .padding(.vertical, 4.5)
        }
        .alert(Text(Localization.areYouDelete), isPresented: $isDelete, actions: {
            HStack {
                Button {
                    isDelete = false
                } label: {
                    Text(Localization.no)
                }

                Button {
                    model.deleteItem(cart: cart)
                    isDelete = false
                } label: {
                    Text(Localization.yes)
                        .foregroundColor(Color.red)
                }
            }
        })

        .padding(.horizontal, 20)
        .frame(maxWidth: .infinity, maxHeight: 85)
        .background(
            Color(NameConstant.Color.ColorF8F9FA)
        )
    }
}
