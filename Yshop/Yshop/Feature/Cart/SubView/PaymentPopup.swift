//
//  PaymentPopup.swift
//  Yshop
//
//  Created by GST.SYN.APL on 25/07/2023.
//

import SwiftUI

struct PaymentPopup: View {
    let width = UIScreen.main.bounds.width
    @Binding var isPresented: Bool
    @ObservedObject var model: CartViewModel

    var body: some View {
        ZStack {
            Color.black.opacity(0.4).edgesIgnoringSafeArea(.all)
            VStack(alignment: .center, spacing: 20) {
                Image(NameConstant.Image.successImage)
                    .resizable()
                    .frame(width: 86, height: 86)
                    .padding(.top, 40)
                Text(Localization.paymenSuccess)
                    .font(
                        Font.customBold(size: 20)
                            .weight(.medium)
                    )
                    .multilineTextAlignment(.center)
                    .foregroundColor(Color(red: 0.1, green: 0.14, blue: 0.19))
                    .frame(width: 159, alignment: .top)

                Button {
                    model.listCart.removeAll()
                    model.getTotalPrice()
                } label: {
                    ZStack {
                        Text(Localization.backToShop)
                            .font(
                                Font.customWMd(size: 18)
                                    .weight(.medium)
                            )
                            .foregroundColor(.white)
                            .padding(.horizontal, 32)
                            .padding(.vertical, 16)
                    }
                    .background(Capsule())
                    .padding(.bottom, 40)
                    .padding(.top, 30)
                }
            }
            .padding(.horizontal, 20)
            .frame(width: width - 40)
            .background(Color.white)
            .cornerRadius(20)
        }
    }
}
