//
//  CheckoutView.swift
//  Yshop
//
//  Created by GST.SYN.APL on 25/07/2023.
//

import SwiftUI
import MapKit

struct CheckoutView: View {
    let width = UIScreen.main.bounds.width
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @ObservedObject var model: CartViewModel
    
    var body: some View {
        VStack(alignment: .leading) {
            HStack(alignment: .center) {
                Button {
                    presentationMode.wrappedValue.dismiss()
                } label: {
                    Image(systemName: "chevron.backward")
                        .myImageModifier()
                        .padding(.leading, 35)
                }
                
                Spacer()
                
                Text(Localization.checkOut)
                    .font(
                        Font.customWMd(size: 16)
                            .weight(.medium)
                    )
                    .foregroundColor(
                        Color(NameConstant.Color.Color1A2530)
                    )
                    .padding(.trailing, 60)
                
                Spacer()
            }
            .padding(.top, 10)
            
            VStack(alignment: .leading) {
                VStack(alignment: .leading, spacing: 16) {
                    HStack {
                        Text(Localization.contactInfor)
                            .font(
                                Font.customBold(size: 14)
                                    .weight(.medium)
                            )
                        Spacer()
                    }
                    
                    HStack {
                        Image(NameConstant.Image.emailIcon)
                            .resizable()
                            .frame(width: 20, height: 20)
                            .background(
                                Color(NameConstant.Color.ColorF8F9FA)
                                    .frame(width: 40, height: 40)
                                    .cornerRadius(12)
                            )
                        
                        VStack(alignment: .leading) {
                            TextField(Localization.email, text: $model.email)
                                .font(Font.customNormal(size: 14))
                                .foregroundColor(
                                    Color(NameConstant.Color.Color1A2530)
                                )
                                .disabled(!model.isEditEmail)
                            
                            Text(Localization.email)
                                .font(Font.customNormal(size: 12))
                                .foregroundColor(
                                    Color(NameConstant.Color.Color707B81)
                                )
                        }
                        .padding(.leading, 12)
                        
                        Spacer()
                        
                        Button {
                            model.isEditEmail.toggle()
                        } label: {
                            if !model.isEditEmail {
                                Image(NameConstant.Image.editIcon)
                                    .foregroundColor(.blue)
                            } else {
                                Image(systemName: "checkmark")
                                    .foregroundColor(.blue)
                            }
                        }
                    }
                    
                    HStack {
                        Image(NameConstant.Image.callIcon)
                            .resizable()
                            .frame(width: 20, height: 20)
                            .background(
                                Color(NameConstant.Color.ColorF8F9FA)
                                    .frame(width: 40, height: 40)
                                    .cornerRadius(12)
                            )
                        
                        VStack(alignment: .leading) {
                            TextField(Localization.phone, text: $model.phone)
                                .font(Font.customNormal(size: 14))
                                .foregroundColor(
                                    Color(NameConstant.Color.Color1A2530)
                                )
                                .keyboardType(.numberPad)
                                .disabled(!model.isEditPhone)
                            
                            Text(Localization.phone)
                                .font(Font.customNormal(size: 12))
                                .foregroundColor(
                                    Color(NameConstant.Color.Color707B81)
                                )
                        }
                        .padding(.leading, 12)
                        
                        Spacer()
                        
                        Button {
                            model.isEditPhone.toggle()
                        } label: {
                            if !model.isEditPhone {
                                Image(NameConstant.Image.editIcon)
                                    .foregroundColor(.blue)
                            } else {
                                Image(systemName: "checkmark")
                                    .foregroundColor(.blue)
                            }
                        }
                    }
                    
                    HStack {
                        Text(Localization.address)
                            .font(
                                Font.customBold(size: 14)
                                    .weight(.medium)
                            )
                        Spacer()
                    }
                    
                    HStack {
                        TextField(Localization.address, text: $model.address)
                            .font(Font.customNormal(size: 12))
                            .foregroundColor(
                                Color(NameConstant.Color.Color707B81)
                            )
                            .disabled(!model.isEditAddress)
                        
                        Spacer()
                        
                        Button {
                            model.isEditAddress.toggle()
                        } label: {
                            if !model.isEditAddress {
                                Image(NameConstant.Image.editIcon)
                                    .foregroundColor(.blue)
                            } else {
                                Image(systemName: "checkmark")
                                    .foregroundColor(.blue)
                            }
                        }
                    }
                    .padding(.top, -4)
                    
                }
                .padding(.horizontal, 20)
                .padding(.top, 16)
                .padding(.bottom, 16)
            }
            .frame(width: width - 40)
            .background(
                Rectangle()
                    .fill(.white)
                    .cornerRadius(16)
            )
            .padding(.horizontal, 20)
            .padding(.top, 24)
            
            Spacer()
            
            VStack(alignment: .leading, spacing: 16) {
                HStack {
                    Text(Localization.subTotal)
                        .font(
                            Font.customWMd(size: 16)
                                .weight(.medium)
                        )
                        .foregroundColor(
                            Color(NameConstant.Color.Color707B81)
                        )
                    Spacer()
                    Text(model.subTotal.convertPrice())
                        .font(
                            Font.customWMd(size: 18)
                                .weight(.medium)
                        )
                        .multilineTextAlignment(.trailing)
                        .foregroundColor(
                            Color(NameConstant.Color.Color1A2530)
                        )
                }
                .padding(.top, 24)
                .padding(.horizontal, 20)
                
                HStack {
                    Text(Localization.shopping)
                        .font(
                            Font.customWMd(size: 16)
                                .weight(.medium)
                        )
                        .foregroundColor(
                            Color(NameConstant.Color.Color707B81)
                        )
                    Spacer()
                    Text(model.shoppingValue.convertPrice())
                        .font(
                            Font.customWMd(size: 18)
                                .weight(.medium)
                        )
                        .multilineTextAlignment(.trailing)
                        .foregroundColor(
                            Color(NameConstant.Color.Color1A2530)
                        )
                }
                .padding(.horizontal, 20)
                
                Divider()
                    .frame(width: width - 40)
                
                HStack {
                    Text(Localization.totalCost)
                        .font(
                            Font.customWMd(size: 16)
                                .weight(.medium)
                        )
                        .foregroundColor(
                            Color(NameConstant.Color.Color1A2530)
                        )
                    
                    Spacer()
                    
                    Text(model.total.convertPrice())
                        .font(
                            Font.customWMd(size: 18)
                                .weight(.medium)
                        )
                        .multilineTextAlignment(.trailing)
                        .foregroundColor(
                            Color(NameConstant.Color.Color1A2530)
                        )
                    
                }
                .padding(.horizontal, 20)
                
                Button {
                    model.invokePayment()
                } label: {
                    ZStack {
                        Capsule()
                            .fill(Color(NameConstant.Color.Color5B9EE1))
                            .frame(height: 52)
                        Text(Localization.payment)
                            .font(
                                Font.customWMd(size: 18)
                                    .weight(.medium)
                            )
                            .foregroundColor(.white)
                    }
                    .padding(.top, 24)
                    .padding(.horizontal, 20)
                }
            }
            .padding(.bottom, 50)
            .background(
                Color.white
            )
            .cornerRadius(24)
            .frame(width: width, height: 206)
        }
        .background(
            Color(NameConstant.Color.ColorF8F9FA)
        )
        .overlay(content: {
            if model.showPopup {
                PaymentPopup(isPresented: $model.showPopup, model: model)
            }
        })
        .alert(Text(Localization.error),
               isPresented: $model.showPopupError,
               actions: {
            VStack {
                Text(Localization.ok)
            }
        }, message: {
            VStack {
                Text(Localization.errorInfor)
            }
        })
        .navigationBarBackButtonHidden(true)
    }
}

struct CheckoutView_Previews: PreviewProvider {
    static var previews: some View {
        CheckoutView(model: CartViewModel())
    }
}
