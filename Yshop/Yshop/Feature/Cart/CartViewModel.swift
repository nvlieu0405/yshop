//
//  CartViewModel.swift
//  Yshop
//
//  Created by GST.SYN.APL on 03/08/2023.
//

import Foundation
import Firebase
import RealmSwift

class CartViewModel: ObservableObject {
    @Published var listCart: [CartModel] = []
    @Published var shoppingValue: Double = 0.0
    @Published var subTotal: Double = 0
    @Published var total: Double = 0
    @Published var count: Int = 0
    @Published var email: String = ""
    @Published var phone: String = ""
    @Published var address: String = ""
    @Published var isEditEmail: Bool = false
    @Published var isEditPhone: Bool = false
    @Published var isEditAddress: Bool = false
    @Published var showPopup: Bool = false
    @Published var showPopupError: Bool = false
    private let ref = Database.database().reference()

    // MARK: - REALM
    private var realm: Realm
    @Published var data: Results<CartRealmData>

    init() {
        do {
            realm = try Realm()
        } catch {
            fatalError("Failed to open Realm: \(error.localizedDescription)")
        }
        data = realm.objects(CartRealmData.self)
    }
    
    func getProfile() {
        let user = Auth.auth().currentUser
        guard let uid = user?.uid else {
            return
        }
        self.email = user?.email ?? ""

        ref.child("\(uid)").getData(completion:  { error, snapshot in
            guard error == nil else {
                print(error!.localizedDescription)
                return
            }

            guard let user = snapshot?.value as? NSDictionary else {
                return
            }

            self.phone = user.value(forKey: "phone") as? String ?? ""
            self.address = user.value(forKey: "address") as? String ?? ""

        })
    }

    func onAppearView() {
        if Auth.auth().currentUser == nil {
            convertDataToCart()
            getTotalPrice()
        } else {
            listCart = []
            getListCartFirebase {
                self.getTotalPrice()
            }
            getProfile()
        }
    }

    func getTotalPrice() {
        subTotal = 0
        total = 0
        shoppingValue = 0
        for item in listCart {
            shoppingValue = 40.0
            subTotal += item.price * Double(item.count)
        }
        total = subTotal + shoppingValue
    }

    func onTapPlus(cart: CartModel) {
        for index in 0..<listCart.count {
            if listCart[index].id == cart.id {
                if listCart[index].count < 10 {
                    listCart[index].count += 1
                    if Auth.auth().currentUser == nil {
                        updateCartToRealm(cart: listCart[index])
                    } else {
                        saveDataInFirebase(id: listCart[index].id, count: listCart[index].count)
                    }
                    getTotalPrice()
                }
            }
        }
    }

    func onTapMinus(cart: CartModel) {
        for index in 0..<listCart.count {
            if listCart[index].id == cart.id {
                if listCart[index].count > 1 {
                    listCart[index].count -= 1
                    if Auth.auth().currentUser == nil {
                        updateCartToRealm(cart: listCart[index])
                    } else {
                        saveDataInFirebase(id: listCart[index].id, count: listCart[index].count)
                    }
                    getTotalPrice()
                }
            }
        }
    }

    func deleteItem(cart: CartModel) {
        listCart = listCart.filter {
            $0.id != cart.id
        }
        if Auth.auth().currentUser == nil {
            deleteCartToRealm(cart: cart)
        } else {
            deleteCartItem(id: cart.id)
        }
        getTotalPrice()
    }

    func getCount() -> Int {
        listCart.count
    }

    func invokePayment() {
        if address.isEmpty || !email.isValidEmail() || !phone.isValidPhoneNumber(){
            showPopupError = true
        } else {
            addCheckout()
        }
    }

    func addCheckout() {
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        let data = CheckoutModel(listProduct: listCart,
                                 address: address,
                                 phone: phone,
                                 email: email)

        ref.child("\(uid)").child("address").setValue(address)
        ref.child("\(uid)").child("phone").setValue(phone)
        ref.child("\(uid)").child("email").setValue(email)

        ref.child("order").child("\(uid)").child("\(data.id)").child("address").setValue("\(data.address)")
        ref.child("order").child("\(uid)").child("\(data.id)").child("phone").setValue(data.phone)
        ref.child("order").child("\(uid)").child("\(data.id)").child("email").setValue(data.email)
        for item in data.listProduct {
            ref.child("order").child("\(uid)").child("\(data.id)").child("\(item.id)").child("count").setValue(item.count)
            ref.child("order").child("\(uid)").child("\(data.id)").child("\(item.id)").child("price").setValue(item.price)
            ref.child("order").child("\(uid)").child("\(data.id)").child("\(item.id)").child("image").setValue(item.image)
            ref.child("order").child("\(uid)").child("\(data.id)").child("\(item.id)").child("name").setValue(item.name)
        }
        clearCartFirebase()
        getTotalPrice()
        showPopup = true
    }

    func getListCartFirebase(completion: @escaping () -> Void) {
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        ref.child("cart").child("\(uid)").getData(completion:  { error, snapshot in
            guard error == nil else {
                print(error!.localizedDescription)
                completion()
                return;
            }
            let listCart = snapshot?.value as? NSDictionary
            guard let listCart = listCart else {
                completion()
                return
            }
            for cartItem in listCart {
                let cart = cartItem.value as? NSDictionary
                let idCart: Int = cart?.value(forKey: "id") as? Int ?? 0
                let name: String = cart?.value(forKey: "name") as? String ?? ""
                let image: String = cart?.value(forKey: "image") as? String  ?? ""
                let price: Double = cart?.value(forKey: "price") as? Double ?? 0
                let count: Int = cart?.value(forKey: "count") as? Int ?? 0
                let size: Int = cart?.value(forKey: "size") as? Int ?? 0

                let cartNew = CartModel(id: idCart,
                                     name: name,
                                     count: count,
                                     price: price,
                                     image: image,
                                     size: size)
                self.listCart.append(cartNew)
            }
            completion()
        })
    }

    func saveDataInFirebase(id: Int, count: Int) {
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        self.ref.child("cart").child("\(uid)").child("id-\(id)").child("count").setValue(count)
    }

    func deleteCartItem(id: Int) {
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        self.ref.child("cart").child("\(uid)").child("\(id)").removeValue()
    }

    func clearCartFirebase() {
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        self.ref.child("cart").child("\(uid)").removeValue()
    }

    func updateCartToRealm(cart: CartModel) {
        do {
            try realm.write {
                for index in 0..<data.count {
                    if data[index].id == cart.id {
                        data[index].count = cart.count
                    }
                }
                convertDataToCart()
            }
        } catch {
            print("Error updating cart: \(error.localizedDescription)")
        }
    }

    func deleteCartToRealm(cart: CartModel) {
        do {
            try realm.write {
                for index in 0..<data.count {
                    if data[index].id == cart.id {
                        realm.delete(data[index])
                    }
                }
                convertDataToCart()
            }
        } catch {
            print("Error deleting cart: \(error.localizedDescription)")
        }
    }

    func deleteAll() {
        do {
            try realm.write {
                realm.deleteAll()
            }
        } catch {
            print("Error deleting cart: \(error.localizedDescription)")
        }
    }

    func convertDataToCart() {
        data = realm.objects(CartRealmData.self)
        listCart = []
        for item in data {
            let cartItem = CartModel(id: item.id,
                                     name: item.name,
                                     count: item.count,
                                     price: item.price,
                                     image: item.image,
                                     size: item.size)
            listCart.append(cartItem)
        }
    }

    func checkAuth() -> Bool {
        if Auth.auth().currentUser == nil {
            return false
        } else {
            return true
        }
    }
}
