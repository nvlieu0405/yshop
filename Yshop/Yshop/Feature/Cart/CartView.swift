//
//  CartView.swift
//  Yshop
//
//  Created by GST.SYN.APL on 14/07/2023.
//

import SwiftUI
import GoogleSignIn

struct CartView: View {
    let width = UIScreen.main.bounds.width
    @ObservedObject private var model = CartViewModel()
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @State var isShowPopup: Bool = false
    @State var goToLogin: Bool = false
    
    var body: some View {
        VStack(alignment: .leading) {
            HStack(alignment: .center) {
                Spacer()
                
                Text(Localization.myCart)
                    .font(
                        Font.customWMd(size: 16)
                            .weight(.medium)
                    )
                    .foregroundColor(
                        Color(NameConstant.Color.Color1A2530)
                    )
                
                
                Spacer()
            }
            .padding(.top, 10)
            
            ScrollView(.vertical, showsIndicators: false) {
                ForEach(model.listCart, id: \.self) { cart in
                    CartItemView(cart: cart, model: model)
                        .padding(.top, 30)
                }
            }
            
            VStack(alignment: .leading, spacing: 16) {
                HStack {
                    Text(Localization.subTotal)
                        .font(
                            Font.customWMd(size: 16)
                                .weight(.medium)
                        )
                        .foregroundColor(
                            Color(NameConstant.Color.Color707B81)
                        )
                    Spacer()
                    Text(model.subTotal.convertPrice())
                        .font(
                            Font.customWMd(size: 18)
                                .weight(.medium)
                        )
                        .multilineTextAlignment(.trailing)
                        .foregroundColor(
                            Color(NameConstant.Color.Color1A2530)
                        )
                }
                .padding(.top, 24)
                .padding(.horizontal, 20)
                
                HStack {
                    Text(Localization.shopping)
                        .font(
                            Font.customWMd(size: 16)
                                .weight(.medium)
                        )
                        .foregroundColor(
                            Color(NameConstant.Color.Color707B81)
                        )
                    Spacer()
                    Text(model.shoppingValue.convertPrice())
                        .font(
                            Font.customWMd(size: 18)
                                .weight(.medium)
                        )
                        .multilineTextAlignment(.trailing)
                        .foregroundColor(
                            Color(NameConstant.Color.Color1A2530)
                        )
                }
                .padding(.horizontal, 20)
                
                Divider()
                    .frame(width: width - 40)
                
                HStack {
                    Text(Localization.totalCost)
                        .font(
                            Font.customWMd(size: 16)
                                .weight(.medium)
                        )
                        .foregroundColor(
                            Color(NameConstant.Color.Color1A2530)
                        )
                    
                    Spacer()
                    
                    Text(model.total.convertPrice())
                        .font(
                            Font.customWMd(size: 18)
                                .weight(.medium)
                        )
                        .multilineTextAlignment(.trailing)
                        .foregroundColor(
                            Color(NameConstant.Color.Color1A2530)
                        )
                    
                }
                .padding(.horizontal, 20)
                
                if model.listCart.isEmpty || !model.checkAuth() {
                    Button {
                        isShowPopup = true
                    } label: {
                        ZStack {
                            Capsule()
                                .fill(Color(NameConstant.Color.Color5B9EE1))
                                .frame(height: 52)
                            Text(Localization.checkOut)
                                .font(
                                    Font.customWMd(size: 18)
                                        .weight(.medium)
                                )
                                .foregroundColor(.white)
                        }
                        .padding(.top, 24)
                        .padding(.horizontal, 20)
                    }
                } else {
                    NavigationLink {
                        CheckoutView(model: model)
                    } label: {
                        ZStack {
                            Capsule()
                                .fill(Color(NameConstant.Color.Color5B9EE1))
                                .frame(height: 52)
                            Text(Localization.checkOut)
                                .font(
                                    Font.customWMd(size: 18)
                                        .weight(.medium)
                                )
                                .foregroundColor(.white)
                        }
                        .padding(.top, 24)
                        .padding(.horizontal, 20)
                    }
                }
            }
            .background(
                Color.white
            )
            .cornerRadius(24)
            .frame(width: width, height: 206)
        }
        .alert(isPresented: $isShowPopup, content: {
            Alert(title: Text(Localization.notification),
                  message: Text(Localization.pleaseLogin),
                  primaryButton: .default(Text(Localization.ok), action: {
                goToLogin = true
            }), secondaryButton: .cancel(Text(Localization.cancel)))
        })
        .background(
            Color(NameConstant.Color.ColorF8F9FA)
        )
        .navigationBarBackButtonHidden(true)
        .padding(.bottom, 20)
        .onAppear {
            model.onAppearView()
        }
        .fullScreenCover(isPresented: $goToLogin) {
            LoginView()
                .preferredColorScheme(.light)
                .onOpenURL { url in
                    GIDSignIn.sharedInstance.handle(url)
                }
        }
    }
}

