//
//  HomeViewModel.swift
//  Yshop
//
//  Created by GST.SYN.APL on 02/08/2023.
//

import SwiftUI

class HomeViewModel: ObservableObject {
    func getListForCategory(_ productList: [ProductElement], categoryIndex: Int) -> [ProductElement] {
        let category = listCategory[categoryIndex]
        return productList.filter {
            $0.category == category
        }
    }

    func getBestChoice(_ productList: [ProductElement], categoryIndex: Int) -> ProductElement? {
        let list = getListForCategory(productList, categoryIndex: categoryIndex)
            .sorted {
                $0.price < $1.price
            }
        return list.first
    }
}
