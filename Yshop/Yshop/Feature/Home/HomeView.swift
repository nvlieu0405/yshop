//
//  HomeView.swift
//  Yshop
//
//  Created by GST.SYN.APL on 14/07/2023.
//

import SwiftUI

struct HomeView: View {
    @EnvironmentObject private var categoryModel: CategoryModel
    @EnvironmentObject private var productList: ProductInApp
    
    @StateObject var model = HomeViewModel()
    
    var body: some View {
        ScrollView(.vertical, showsIndicators: false) {
            VStack(alignment: .leading) {
                NavigationLink {
                    SearchView()
                } label: {
                    HomeSearchView()
                }
                .padding(.horizontal, 20)
                .padding(.top, 30)
                
                CategoryView()
                    .frame(width: UIScreen.main.bounds.width - 40)
                    .padding(.top, 32)
                    .padding(.horizontal, 20)
                
                
                HomeSectionView(title: Localization.popularShoes)
                    .padding(.top, 24)
                    .padding(.horizontal, 20)
                
                
                HomePopularGridView()
                    .padding(.top, 16)
                    .padding(.horizontal, 20)
                
                
                HStack {
                    Text(Localization.bestChoice)
                        .font(
                            Font.customBold(size: 16)
                                .weight(.medium)
                        )
                        .foregroundColor(Color(NameConstant.Color.Color1A2530))
                    
                    Spacer()
                }
                .padding(.top, 24)
                .padding(.horizontal, 20)
                
                if let product = model.getBestChoice(productList.productList, categoryIndex: categoryModel.selectedIndex) {
                    NavigationLink {
                        DetailView(product: product)
                    } label: {
                        HomeBestChoiceView(product: product)
                    }
                    .padding(.horizontal, 20)
                }
            }
        }
        .background(Color(NameConstant.Color.ColorF8F9FA))
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView(model: HomeViewModel())
            .environmentObject(ProductInApp())
            .environmentObject(CategoryModel())
    }
}
