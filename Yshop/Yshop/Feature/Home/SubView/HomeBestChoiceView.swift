//
//  HomeBestChoiceView.swift
//  Yshop
//
//  Created by GST.SYN.APL on 17/07/2023.
//

import SwiftUI
import Kingfisher

struct HomeBestChoiceView: View {
    let product: ProductElement
    
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Text(Localization.bestChoice)
                    .font(
                        Font.customWMd(size: 12)
                            .weight(.medium)
                    )
                    .kerning(0.96)
                    .foregroundColor(Color(NameConstant.Color.Color5B9EE1))
                
                Text(product.name)
                    .font(
                        Font.customWMd(size: 20)
                            .weight(.medium)
                    )
                    .multilineTextAlignment(.leading)
                    .foregroundColor(Color(NameConstant.Color.Color1A2530))
                    .padding(.top, 4)
                
                Text(product.price.convertPrice())
                    .font(
                        Font.customWMd(size: 16)
                            .weight(.medium)
                    )
                    .foregroundColor(Color(NameConstant.Color.Color1A2530))
                    .padding(.top, 10)
            }
            
            Spacer(minLength: 17)
            
            KFImage(URL(string: product.gallery[0]))
                .placeholder({ _ in
                    ProgressView()
                        .progressViewStyle(CircularProgressViewStyle())
                })
                .resizable()
                .frame(width: 114, height: 60)
                .aspectRatio(contentMode: .fill)
                .clipped()
                .rotationEffect(Angle(degrees: -14.5))
        }
        .padding(20)
        .frame(width: UIScreen.main.bounds.width - 40, alignment: .center)
        .background(.white)
        .cornerRadius(16)
    }
}
