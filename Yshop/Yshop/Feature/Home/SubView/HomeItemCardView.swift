//
//  HomeItemCardView.swift
//  Yshop
//
//  Created by GST.SYN.APL on 14/07/2023.
//

import SwiftUI
import Kingfisher

struct HomeItemCardView: View {

    let product: ProductElement

    var body: some View {
        VStack(alignment: .leading, spacing: 12) {
            KFImage(URL(string: product.gallery[0]))
                .placeholder({ _ in
                    ProgressView()
                        .progressViewStyle(CircularProgressViewStyle())
                })
                .resizable()
                .scaledToFit()
                .frame(width: 100)
                .frame(maxHeight: 60)
                .clipped()
                .padding(.bottom, 20)
                .padding(.top, 20)
                .padding(.leading, 0)
                .padding(.trailing, 0)
                .background {
                    Rectangle()
                        .fill(.white)
                        .frame(width: (UIScreen.main.bounds.width - 84)/2, height: 80)
                }
            

            Text(Localization.bestSeller)
                .font(Font.customWMd(size: 12))
                .foregroundColor(Color(red: 0.36, green: 0.62, blue: 0.88))
                .padding(.leading, 0)

            Text(product.name.capitalized)
                .font(Font.customWMd(size: 16).weight(.medium))
                .foregroundColor(.black.opacity(0.6))
                .kerning(0.25)
                .padding(.leading, 0)
                .lineLimit(2)
                .multilineTextAlignment(.leading)
                .padding(.trailing, 12)

            HStack {
                Text(product.price.convertPrice())
                    .font(Font.customWMd(size: 14).weight(.medium))
                    .foregroundColor(.black)
                    .padding(.top, 12)
                    .padding(.leading, 0)
                Spacer()
                Image(NameConstant.Image.addIcon)
                    .resizable()
                    .frame(width: 34, height: 34)
            }
        }
        .frame(width: (UIScreen.main.bounds.width - 84)/2)
        .padding(.leading, 12)
        .padding(.trailing, 0)
        .padding(.top, 12)
        .padding(.bottom, 0)
        .background(.white)
        .cornerRadius(16)
    }
}
