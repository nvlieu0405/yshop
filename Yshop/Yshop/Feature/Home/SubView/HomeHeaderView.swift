//
//  HomeHeaderView.swift
//  Yshop
//
//  Created by GST.SYN.APL on 17/07/2023.
//

import SwiftUI

struct HomeHeaderView: View {
    var body: some View {
        HStack(alignment: .center) {
            Image(NameConstant.Image.menuIcon)
                .resizable()
                .frame(width: 14, height: 14, alignment: .center)
                .background(
                    Rectangle()
                        .fill(Color.white)
                        .frame(width: 44, height: 44, alignment: .center)
                        .cornerRadius(40)
                )
            Spacer()

            VStack {
                Text(Localization.storeLocation)
                .font(Font.customWMd(size: 12))
                .foregroundColor(Color(NameConstant.Color.Color707B81))

                HStack {
                    Image(NameConstant.Image.locationIcon)
                        .resizable()
                        .frame(width: 14, height: 14)
                    Text(Localization.locationDefault)
                    .font(
                    Font.customWMd(size: 14)
                    .weight(.medium)
                    )
                    .foregroundColor(Color(red: 0.1, green: 0.14, blue: 0.19))
                }
            }

            Spacer()

            NavigationLink {
                CartView()
            } label: {
                Image(NameConstant.Image.cartIcon)
                    .frame(width: 14, height: 14, alignment: .center)
                    .background(
                        Rectangle()
                            .fill(Color.white)
                            .frame(width: 44, height: 44, alignment: .center)
                            .cornerRadius(40)
                    )
                    .padding(.trailing, 0)
            }
        }
    }
}

struct HomeHeaderView_Previews: PreviewProvider {
    static var previews: some View {
        HomeHeaderView()
            .previewLayout(.fixed(width: 335, height: 44))
            .preferredColorScheme(.light)
    }
}
