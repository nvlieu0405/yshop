//
//  HomeSectionView.swift
//  Yshop
//
//  Created by GST.SYN.APL on 17/07/2023.
//

import SwiftUI

struct HomeSectionView: View {
    @ObservedObject private var homeViewModel = HomeViewModel()
    @EnvironmentObject var product: ProductInApp
    @EnvironmentObject var categoryModel: CategoryModel
    let title: String

    var body: some View {
        HStack {
            Text(title)
                .font(
                    Font.customBold(size: 16)
                        .weight(.medium)
                )
                .foregroundColor(Color(NameConstant.Color.Color1A2530))

            Spacer()

            NavigationLink {
                BestSellerView(productList: homeViewModel.getListForCategory(product.productList, categoryIndex: categoryModel.selectedIndex))
            } label: {
                Text(Localization.seeAll)
                    .font(Font.customWMd(size: 13))
                    .multilineTextAlignment(.trailing)
                    .foregroundColor(Color(NameConstant.Color.Color5B9EE1))
            }
        }
    }
}

struct HomeSectionView_Previews: PreviewProvider {
    static var previews: some View {
        HomeSectionView(title: Localization.popularShoes)
    }
}
