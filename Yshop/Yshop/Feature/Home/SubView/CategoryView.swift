//
//  CategoryView.swift
//  Yshop
//
//  Created by GST.SYN.APL on 14/07/2023.
//

import SwiftUI

struct CategoryView: View {
    // MARK: - PROPERTIES
    @EnvironmentObject var model: CategoryModel

    let list: [CategoryType] = listCategory
    @State private var selectedID: Int = 0

    var body: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            HStack(alignment: .center, spacing: 16) {
                ForEach(0..<list.count, id: \.self) { id in
                    CategoryItemView(category: list[id], selected: selectedID == id)
                        .onTapGesture {
                            selectedID = id
                            model.selectedIndex = id
                        }
                        .fixedSize()
//                        .padding(.trailing, 16)
                }
            }
            .frame(width: UIScreen.main.bounds.width - 40)
            .padding(.horizontal, 0)
            .padding(.vertical, 20)
        }
        .onAppear {
            selectedID = model.selectedIndex
        }
    }
}

struct CategoryView_Previews: PreviewProvider {
    static var previews: some View {
        CategoryView()
            .preferredColorScheme(.dark)
            .environmentObject(CategoryModel())
    }
}

