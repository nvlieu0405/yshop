//
//  CategoryItemView.swift
//  Yshop
//
//  Created by GST.SYN.APL on 24/07/2023.
//

import SwiftUI

struct CategoryItemView: View {
    let category: CategoryType
    var selected: Bool = true
    var body: some View {
        if selected {
            HStack(alignment: .center, spacing: 8) {
                Image(category.getImageName())
                    .resizable()
                    .frame(width: 24, height: 24)
                    .background(
                        Circle()
                            .fill(.white)
                            .frame(width: 32, height: 32)
                    )
                    .padding(.leading, 6)

                Text(category.rawValue.capitalized)
                    .font(
                        Font.customBold(size: 14)
                            .weight(.medium)
                    )
                    .foregroundColor(.white)
                    .lineLimit(1)
            }
            .padding(6)
            .background(
                Capsule()
                    .fill(Color.blue)
                    .frame(height: 44)
            )
            .frame(maxWidth: .infinity)
        } else {
            Image(category.getImageName())
                .resizable()
                .frame(width: 24, height: 24)
                .background(
                    Circle()
                        .fill(.white)
                        .frame(width: 44, height: 44)
                )
                .padding(6)
        }
    }
}

struct CategoryItemView_Previews: PreviewProvider {
    static var previews: some View {
        CategoryItemView(category: listCategory[0])
            .preferredColorScheme(.dark)
    }
}
