//
//  HomeSearchView.swift
//  Yshop
//
//  Created by GST.SYN.APL on 17/07/2023.
//

import SwiftUI

struct HomeSearchView: View {
    let width = UIScreen.main.bounds.width

    var body: some View {
        HStack {
            Image(NameConstant.Image.searchIcon)
                .resizable()
                .frame(width: 18, height: 18)
                .padding()

            Text(Localization.searchText)
                .foregroundColor(
                    Color(NameConstant.Color.Color707B81)
                )
                .padding(.leading, -10)
            Spacer()
        }
        .background(
            Capsule()
                .fill(Color(NameConstant.Color.ColorEEEEEE))
        )
    }
}

struct HomeSearchView_Previews: PreviewProvider {
    static var previews: some View {
        HomeSearchView()
    }
}
