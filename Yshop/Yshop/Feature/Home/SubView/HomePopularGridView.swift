//
//  HomePopularGridView.swift
//  Yshop
//
//  Created by GST.SYN.APL on 14/07/2023.
//

import SwiftUI

struct HomePopularGridView: View {
    let gridItem: [GridItem] = [.init(.flexible())]
    @EnvironmentObject var productListApp: ProductInApp
    @EnvironmentObject var model: CategoryModel
    @ObservedObject private var homeViewModel = HomeViewModel()

    var body: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            HStack(spacing: 20) {
                ForEach(0..<homeViewModel.getListForCategory(productListApp.productList, categoryIndex: model.selectedIndex).count, id: \.self) { index
                    in
                    if index < 4 && !homeViewModel.getListForCategory(productListApp.productList, categoryIndex: model.selectedIndex).isEmpty {
                        NavigationLink(destination:
                                        DetailView(product: homeViewModel.getListForCategory(productListApp.productList, categoryIndex: model.selectedIndex)[index], size: nil)
                                       ,
                                       label: {
                            HomeItemCardView(product: homeViewModel.getListForCategory(productListApp.productList, categoryIndex: model.selectedIndex)[index])

                        })
                        .buttonStyle(PlainButtonStyle())
                    }
                }
            }
        }
    }
}

struct HomePopularGridView_Previews: PreviewProvider {
    static var previews: some View {
        HomePopularGridView()
            .environmentObject(ProductInApp())
            .environmentObject(CategoryModel())
    }
}
