//
//  TabbarView.swift
//  Yshop
//
//  Created by GST.SYN.APL on 14/07/2023.
//

import SwiftUI

struct TabbarView: View {
    @Binding var selectedIndex: Int
    let items: [TabBarItem] = tabItems
    let index = 0

    var body: some View {
        HStack(alignment: .bottom) {
            HStack(spacing: 70) {
                ForEach(0..<items.count, id: \.self) { index in
                    Image(selectedIndex == index ? items[index].imageSelected : items[index].imageName)
                        .resizable()
                        .frame(width: 24, height: 24)
//                        .foregroundColor(selectedIndex == index ? .blue : .gray)
                        .onTapGesture {
                            selectedIndex = index
                        }
                }
            }
            .padding(.horizontal, 10)
        }
        .frame(maxWidth: .infinity, maxHeight: 60)
        .background(
            CustomBackground()
                .fill(
                    Color(NameConstant.Color.ColorF8F9FA)
                )
                .frame(width: UIScreen.main.bounds.width)
        )
        .background(.white)
    }
}

struct TabbarView_Previews: PreviewProvider {
    static var previews: some View {
        let selectedIndex = Binding<Int> (
            get: { 0 },
            set: { _ in }
        )
        TabbarView(selectedIndex: selectedIndex)
    }
}

struct CustomBackground: Shape {
    func path(in rect: CGRect) -> Path {
        var path = Path()
        path.move(to: CGPoint(x: 0, y: 0))
        path.addCurve(
            to: CGPoint(x: rect.width / 2, y: 20),
            control1: CGPoint(x: rect.width / 2 - 40, y: 0),
            control2: CGPoint(x: rect.width / 2 - 40, y: 20)
        )

        path.addCurve(
            to: CGPoint(x: rect.width, y: 0),
            control1: CGPoint(x: rect.width / 2 + 40, y: 20),
            control2: CGPoint(x: rect.width / 2 + 40, y: 0)
        )

        path.closeSubpath()

        return path
    }
}

