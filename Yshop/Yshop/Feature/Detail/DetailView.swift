//
//  DetailView.swift
//  Yshop
//
//  Created by GST.SYN.APL on 17/07/2023.
//

import SwiftUI
import Kingfisher

struct DetailView: View {
    let gridItem: [GridItem] = [GridItem.init(.flexible())]
    let product: ProductElement

    @ObservedObject private var model = DetailViewModel()
    @State private var isShowAlert: Bool = false
    @State private var selectedIndex: Int = 0
    @State var size: Int?
    @State var isShowWarning: Bool = false

    var body: some View {
        ZStack {
            VStack(alignment: .leading, spacing: 0) {
                DetailHeaderView()
                    .frame(height: 44)
                    .padding(.horizontal, 30)
                ZStack {
                    KFImage(URL(string: product.gallery[selectedIndex]))
                        .placeholder({ _ in
                            ProgressView()
                                .progressViewStyle(CircularProgressViewStyle())
                        })
                        .resizable()
                        .scaledToFit()
                        .frame(width: UIScreen.main.bounds.width - 40, alignment: .center)
                        .frame(maxHeight: 150)
                }
                .frame(height: 202)
                .padding(.horizontal, 32)
                .background(
                    Rectangle()
                        .fill(.white)
                        .frame(width: UIScreen.main.bounds.width - 40, alignment: .center)
                        .cornerRadius(16)
                        .padding(.vertical, 16)
                )

                ScrollView(.vertical, showsIndicators: false) {
                    VStack(alignment: .leading) {
                        Text(Localization.bestSeller)
                            .font(Font.customWMd(size: 14))
                            .foregroundColor(
                                Color(NameConstant.Color.Color5B9EE1)
                            )
                            .padding(.horizontal, 20)
                            .padding(.top, 0)

                        Text(product.name)
                            .font(
                                Font.customWMd(size: 24)
                                    .weight(.medium)
                            )
                            .foregroundColor(
                                Color(NameConstant.Color.Color1A2530)
                            )
                            .padding(.top, 6)
                            .padding(.horizontal, 20)

                        Text(product.price.convertPrice())
                            .font(
                                Font.customWMd(size: 20)
                                    .weight(.medium)
                            )
                            .foregroundColor(
                                Color(NameConstant.Color.Color1A2530)
                            )
                            .padding(.top, 0)
                            .padding(.horizontal, 20)

                        Text(product.category.getDescription())
                            .font(Font.customWMd(size: 14))
                            .foregroundColor(
                                Color(NameConstant.Color.Color707B81)
                            )
                            .frame(maxWidth: UIScreen.main.bounds.width - 80, alignment: .topLeading)
                            .multilineTextAlignment(.leading)
                            .padding(.top, 8)
                            .padding(.horizontal, 20)

                        Text(Localization.gallery)
                            .modifier(Title())

                        LazyHGrid(rows: gridItem) {
                            ForEach(0..<product.gallery.count, id: \.self) { index in
                                Button {
                                    if selectedIndex != index {
                                        selectedIndex = index
                                    }
                                } label: {
                                    DetailGalleryView(urlImage: product.gallery[index])
                                        .overlay {
                                            RoundedRectangle(cornerRadius: 12)
                                                .stroke(selectedIndex == index ? .blue : Color.gray.opacity(0.5), lineWidth: 1)
                                        }
                                }
                            }
                        }
                        .padding(.leading, 20)

                        Text(Localization.size)
                            .modifier(Title())

                        LazyHGrid(rows: gridItem, alignment: .center, spacing: 13) {
                            ForEach(38..<43) { size in
                                Button {
                                    self.size = size
                                } label: {
                                    ZStack(alignment: .center) {
                                        Circle()
                                            .fill(
                                                self.size == size ? Color(NameConstant.Color.Color5B9EE1) : Color(NameConstant.Color.ColorF8F9FA)
                                            )
                                            .frame(width: 45, height: 45)
                                            .shadow(color: Color(NameConstant.Color.Color5B9EE1).opacity(0.4), radius: 8, x: 0, y: self.size == size ? 8 : 0)
                                        Text("\(size)")
                                            .foregroundColor(
                                                self.size == size ? Color.white : Color(NameConstant.Color.Color707B81)
                                            )
                                    }
                                    .foregroundColor(.black)
                                }
                            }
                        }
                        .padding(.horizontal, 20)

                        Spacer()
                    }
                    .padding(.top, 16)
                    .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - 150, alignment: .leading)
                    .background(
                        Color.white.clipShape(CustomShape())
                    )
                }
            }
            .padding(.leading, 20)
            .padding(.top, 8)
            .background(
                Color(NameConstant.Color.ColorF8F9FA)
            )

            HStack(alignment: .top) {
                VStack(alignment: .leading) {
                    Text(Localization.price)
                        .font(Font.customWMd(size: 16))
                        .foregroundColor(
                            Color(NameConstant.Color.Color707B81)
                        )

                    Text(product.price.convertPrice())
                        .font(
                            Font.customWMd(size: 20)
                                .weight(.medium)
                        )
                        .foregroundColor(
                            Color(NameConstant.Color.Color1A2530)
                        )
                }
                .padding(.horizontal, 20)

                Spacer()

                Button {
                    if let size = size {
                        model.updateListCart(product: product,
                                             size: size,
                                             selectedIndex: selectedIndex) {
                            isShowAlert = true
                        }
                    } else {
                        isShowWarning = true
                    }
                } label: {
                    ZStack {
                        Text(Localization.addToCart)
                            .font(
                                Font.customWMd(size: 18)
                                    .weight(.medium)
                            )
                            .foregroundColor(.white)
                            .padding(.horizontal, 32)
                            .padding(.vertical, 16)
                    }
                    .background(Capsule())
                    .padding(.trailing, 20)
                }
            }
            .frame(width: UIScreen.main.bounds.width)
            .padding(.top, 16)
            .padding(.bottom, 24)
            .background(.white)
            .cornerRadius(24)
            .shadow(color: Color(NameConstant.Color.Color707B81).opacity(0.12), radius: 2, x: -1.5, y: 0)
            .offset(y: UIScreen.main.bounds.height / 2 - 47)
        }
        .alert(Text(Localization.error),
               isPresented: $isShowWarning,
               actions: {
            VStack {
                Text(Localization.ok)
            }
        }, message: {
            Text(Localization.pleaseChooseSize)
        })
        .alert(Text(Localization.success),
               isPresented: $isShowAlert,
               actions: {
            VStack {
                Button {
                    isShowAlert = false
                } label: {
                    Text(Localization.ok)
                }
            }
        }, message: {
            Text(Localization.successContent)
        })
        .navigationBarBackButtonHidden(true)
    }
}

struct Title: ViewModifier {
    func body(content: Content) -> some View {
        content
            .font(
                Font.customWMd(size: 18)
                    .weight(.medium)
            )
            .foregroundColor(
                Color(NameConstant.Color.Color1A2530)
            )
            .padding(.leading, 20)
            .padding(.top, 16)
    }
}
