//
//  DetailGalleryView.swift
//  Yshop
//
//  Created by GST.SYN.APL on 17/07/2023.
//

import SwiftUI
import Kingfisher

struct DetailGalleryView: View {
    let urlImage: String
    var body: some View {
        ZStack {
            Rectangle()
                .fill(.white)
                .frame(width: 56, height: 56)
                .cornerRadius(12)
            
            KFImage(URL(string: urlImage))
                .placeholder({ _ in
                    ProgressView()
                        .progressViewStyle(CircularProgressViewStyle())
                })
                .resizable()
                .scaledToFit()
                .frame(width: 40, height: 40, alignment: .center)
                .rotationEffect(Angle(degrees: -13.5))
        }
        .background(.white)
        .frame(width: 56, height: 56, alignment: .center)
        .cornerRadius(12)
    }
}

struct DetailGalleryView_Previews: PreviewProvider {
    static var previews: some View {
        DetailGalleryView(urlImage: "https://authentic-shoes.com/wp-content/uploads/2023/04/upload_058fbbc3c8dc4b6085d8617eef5083ad.jpg")
    }
}
