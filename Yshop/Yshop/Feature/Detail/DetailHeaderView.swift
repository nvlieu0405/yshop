//
//  DetailHeaderView.swift
//  Yshop
//
//  Created by GST.SYN.APL on 17/07/2023.
//

import SwiftUI

struct DetailHeaderView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    var body: some View {
        HStack {
            Button {
                presentationMode.wrappedValue.dismiss()
            } label: {
                Image(systemName: "chevron.backward")
                    .myImageModifier()
            }

            Spacer()
            
            Text(Localization.menShoes)
                .font(
                    Font.customWMd(size: 16)
                        .weight(.medium)
                )
                .foregroundColor(Color(NameConstant.Color.Color1A2530))
            Spacer()
        }
        
    }
}

struct DetailHeaderView_Previews: PreviewProvider {
    static var previews: some View {
        DetailHeaderView()
    }
}
