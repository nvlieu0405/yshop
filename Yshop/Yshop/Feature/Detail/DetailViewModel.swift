//
//  DetailViewModel.swift
//  Yshop
//
//  Created by GST.SYN.APL on 02/08/2023.
//

import SwiftUI
import FirebaseDatabase
import Firebase
import RealmSwift

class DetailViewModel: ObservableObject {
    private let ref = Database.database().reference()
    private var isExist: Bool = false
    private var count: Int = 1
    @Published var listCart: [CartModel] = []

    // MARK: - REALM
    private var realm: Realm
    @Published var data: Results<CartRealmData>


    init() {
        do {
            realm = try Realm()
        } catch {
            fatalError("Failed to open Realm: \(error.localizedDescription)")
        }
        data = realm.objects(CartRealmData.self)
    }
    
    func getListCartFirebase(id: Int, completion: @escaping (Bool) -> Void) {
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        ref.child("cart").child("\(uid)").getData(completion:  { error, snapshot in
            guard error == nil else {
                print(error!.localizedDescription)
                return;
            }
            let listCart = snapshot?.value as? [NSDictionary]
            guard let listCart = listCart else {
                completion(false)
                return
            }
            for cart in listCart {
                let idCart: Int = cart.value(forKey: "id") as? Int ?? 0
                if id == idCart {
                    self.count = cart.value(forKey: "count") as? Int ?? 0
                    completion(true)
                }
            }
            completion(false)
        })
    }

    func updateListCart(product: ProductElement, size: Int, selectedIndex: Int, completion: @escaping () -> Void) {
        if let uid = Auth.auth().currentUser?.uid {
            getListCartFirebase(id: product.id) { isExist in
                if isExist {
                    self.count += 1
                    self.ref.child("cart").child("\(uid)").child("id-\(product.id)").child("count").setValue(self.count)
                } else {
                    self.ref.child("cart").child("\(uid)").child("id-\(product.id)").child("id").setValue(product.id)
                    self.ref.child("cart").child("\(uid)").child("id-\(product.id)").child("name").setValue(product.name)
                    self.ref.child("cart").child("\(uid)").child("id-\(product.id)").child("image").setValue(product.gallery[selectedIndex])
                    self.ref.child("cart").child("\(uid)").child("id-\(product.id)").child("price").setValue(product.price)
                    self.ref.child("cart").child("\(uid)").child("id-\(product.id)").child("count").setValue(self.count)
                    self.ref.child("cart").child("\(uid)").child("id-\(product.id)").child("size").setValue(size)
                }
                completion()
            }
        } else {
            addCartToRealm(product: product, size: size, selectedIndex: selectedIndex) {
                completion()
            }
        }
    }

    func addCartToRealm(product: ProductElement, size: Int, selectedIndex: Int, completion: @escaping () -> Void) {
        do {
            try realm.write {
                if data.isEmpty {
                    let newRealm = CartRealmData()
                    newRealm.id = product.id
                    newRealm.name = product.name
                    newRealm.image = product.gallery[selectedIndex]
                    newRealm.count = 1
                    newRealm.size = size
                    newRealm.price = product.price
                    realm.add(newRealm)
                    completion()
                } else {
                    for index in 0..<data.count {
                        if data[index].id == product.id {
                            data[index].count += 1
                            completion()
                            return
                        }
                    }
                    let newRealm = CartRealmData()
                    newRealm.id = product.id
                    newRealm.name = product.name
                    newRealm.image = product.gallery[selectedIndex]
                    newRealm.count = 1
                    newRealm.size = size
                    newRealm.price = product.price
                    realm.add(newRealm)
                    completion()
                }
            }
        } catch {
            print("Error adding cart: \(error.localizedDescription)")
        }
    }
}
