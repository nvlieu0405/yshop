//
//  SplashView.swift
//  Yshop
//
//  Created by GST.SYN.APL on 21/07/2023.
//

import SwiftUI
import GoogleSignIn

struct SplashView: View {
    private let width = UIScreen.main.bounds.width
    private let height = UIScreen.main.bounds.height
    @AppStorage("isFirstRun") private var isFirstRun: Bool?

    @StateObject private var spashViewModel = SplashViewModel()

    var body: some View {
        NavigationView {
            ZStack {
                Circle()
                    .fill(Color.white)
                    .frame(width: 400, height: 400)
                    .background(
                        Circle()
                            .fill(Color(NameConstant.Color.Color5B9EE1).opacity(0.15))
                            .frame(width: 480, height: 480)
                    )
                    .offset(x: width / 2 + 10, y: -height/2 + 10)
                VStack {
                    ZStack {
                        Text(Localization.nike.uppercased())
                            .font(Font.customBold(size: 160))
                            .foregroundColor(
                                Color(NameConstant.Color.Color707B81)
                                    .opacity(0.25)
                            )
                            .offset(y: 60)

                        spashViewModel.imageSplash
                            .resizable()
                            .scaledToFit()
                            .frame(height: 321)
                            .rotationEffect(Angle(degrees: -12.5))
                    }
                    .padding(.top, 89)

                    VStack {
                        Text(spashViewModel.title)
                            .font(
                                Font.customWMd(size: 40)
                                    .weight(.medium)
                            )
                            .foregroundColor(Color(NameConstant.Color.Color1A2530))
                            .frame(width: width - 40,alignment: .topLeading)
                            .padding(.horizontal, 20)

                        Text(spashViewModel.description)
                            .font(Font.customWMd(size: 20))
                            .foregroundColor(Color(NameConstant.Color.Color707B81))
                            .frame(width: width - 40, alignment: .topLeading)
                    }
                    Spacer()
                }

                VStack {
                    Spacer()
                    HStack {
                        spashViewModel.imageStep

                        Spacer()

                        Button {
                            spashViewModel.tapOnButton()
                        } label: {
                            if spashViewModel.step == .three {
                                NavigationLink {
                                    LoginView()
                                        .preferredColorScheme(.light)
                                        .onOpenURL { url in
                                            GIDSignIn.sharedInstance.handle(url)
                                        }
                                } label: {
                                    CapsuleButtonView(text: spashViewModel.titleButton)
                                }

                            } else {
                                CapsuleButtonView(text: spashViewModel.titleButton)
                            }
                        }

                    }
                }
                .padding(.horizontal, 20)
            }
        }
        .navigationBarBackButtonHidden(true)
    }
}

struct SplashView_Previews: PreviewProvider {
    static var previews: some View {
        SplashView()
    }
}
