//
//  SplashViewModel.swift
//  Yshop
//
//  Created by GST.SYN.APL on 21/07/2023.
//

import SwiftUI

enum StepSplash {
    case one
    case two
    case three
}

class SplashViewModel: ObservableObject {
    @Published var step: StepSplash = .one
    @Published var title: String = ""
    @Published var description: String = ""
    @Published var imageStep: Image = Image(NameConstant.Image.firstStep)
    @Published var titleButton: String = Localization.getStarted
    @Published var imageSplash: Image = Image(NameConstant.Image.imageSplash1)

    init() {
        getContent()
    }
    
    private func getContent() {
        switch step {
        case .one:
            title = Localization.title1
            description = Localization.description1
            imageStep = Image(NameConstant.Image.firstStep)
            titleButton = Localization.getStarted
            imageSplash = Image(NameConstant.Image.imageSplash1)
        case .two:
            title = Localization.title2
            description = Localization.description2
            imageStep = Image(NameConstant.Image.secondStep)
            titleButton = Localization.next
            imageSplash = Image(NameConstant.Image.imageSplash2)
        case .three:
            title = Localization.title3
            description = Localization.description3
            imageStep = Image(NameConstant.Image.thirdStep)
            titleButton = Localization.next
            imageSplash = Image(NameConstant.Image.imageSplash3)
        }
    }

    func tapOnButton() {
        switch step {
        case .one:
            step = .two
        case .two:
            step = .three
        case .three:
            break
        }
        getContent()
    }

}
