//
//  FavoriteView.swift
//  Yshop
//
//  Created by GST.SYN.APL on 14/07/2023.
//

import SwiftUI

struct FavoriteView: View {
    let gridItem: [GridItem] = [.init(.flexible()), .init(.flexible())]
    @EnvironmentObject var product: ProductInApp
    @ObservedObject private var model = FavoriteViewModel()
    
    var body: some View {
        VStack {
            FavoriteHeaderView()
                .padding(.top, 8)
            Spacer()

            ScrollView(.vertical, showsIndicators: false) {
                LazyVGrid(columns: gridItem, spacing: 20) {
                    ForEach(0..<product.productList.count, id: \.self) { index in
                        NavigationLink {
                            DetailView(product: product.productList[index])
                        } label: {
                            FavoriteItemView(product: product.productList[index],
                                             isFavoriteView: true,
                                             model: model)
                        }
                    }
                }
            }
            .padding(.top, 24)
            
        }
        .background(
            Color(NameConstant.Color.ColorF8F9FA)
        )
        .onAppear {
            model.onAppear {}
        }
//        .onDisappear {
//            model.updateDataFirebase()
//        }
    }
}

struct FavoriteView_Previews: PreviewProvider {
    static var previews: some View {
        FavoriteView()
    }
}
