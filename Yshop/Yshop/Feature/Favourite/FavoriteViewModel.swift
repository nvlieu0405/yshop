//
//  FavoriteViewModel.swift
//  Yshop
//
//  Created by GST.SYN.APL on 07/08/2023.
//

import Foundation
import Firebase
import RealmSwift

class FavoriteViewModel: ObservableObject {
    private let ref = Database.database().reference()
    let uid = Auth.auth().currentUser?.uid
    @Published var listFavorite: [Int] = []

    // MARK: - REALM
    private var realm: Realm
    @Published var data: Results<FavoriteRealmData>

    init() {
        do {
            realm = try Realm()
        } catch {
            fatalError("Failed to open Realm: \(error.localizedDescription)")
        }
        data = realm.objects(FavoriteRealmData.self)
    }
    
    func getListFavorite(completion: @escaping () -> Void) {
        guard let uid = uid else {
            return
        }
        ref.child("favorite").child("\(String(describing: uid))").getData(completion:  { error, snapshot in
            guard error == nil else {
                print(error!.localizedDescription)
                return
            }
            let favorite = snapshot?.value as? [Int]
            self.listFavorite = favorite ?? []
            completion()
        })
    }

    func updateListFavorite(id: Int, isFavorite: Bool) {
        if isFavorite {
            listFavorite.append(id)
        } else {
            listFavorite.removeAll { index in
                index == id
            }
        }

        updateDataFirebase()
    }

    func updateDataFirebase() {
        if let uid = uid {
            ref.child("favorite").child("\(uid)").removeValue()
            ref.child("favorite").updateChildValues(["\(String(describing: uid))": listFavorite])
        } else {
            do {
                try realm.write {
                    realm.deleteAll()
                    for item in listFavorite {
                        let newRealm = FavoriteRealmData()
                        newRealm.id = item
                        realm.add(newRealm)
                    }
                }
            } catch {
                print("Error updating cart: \(error.localizedDescription)")
            }
        }
    }

    func onAppear(completion: @escaping () -> Void) {
        if let uid = uid {
            getListFavorite {
                completion()
            }
        } else {
            listFavorite = []
            for item in data {
                listFavorite.append(item.id)
            }
            completion()
        }
    }

    func checkFavorite(id: Int) -> Bool {
        return listFavorite.contains(id)
    }
}
