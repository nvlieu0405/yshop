//
//  FavoriteHeaderView.swift
//  Yshop
//
//  Created by GST.SYN.APL on 25/07/2023.
//

import SwiftUI

struct FavoriteHeaderView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    var body: some View {
        HStack {
            Spacer()

            Text(Localization.favourite)
                .font(
                    Font.customWMd(size: 16)
                        .weight(.medium)
                )
                .foregroundColor(
                    Color(NameConstant.Color.Color1A2530)
                )

            Spacer()
        }
        .padding(.horizontal, 35)
    }
}

struct FavoriteHeaderView_Previews: PreviewProvider {
    static var previews: some View {
        FavoriteHeaderView()
    }
}
