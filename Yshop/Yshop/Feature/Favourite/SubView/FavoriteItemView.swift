//
//  FavoriteItemView.swift
//  Yshop
//
//  Created by GST.SYN.APL on 25/07/2023.
//

import SwiftUI
import Kingfisher

struct FavoriteItemView: View {
    @State var product: ProductElement
    let isFavoriteView: Bool
    @ObservedObject var model: FavoriteViewModel

    var body: some View {
        VStack {
            VStack {
                ZStack {
                    VStack {
                        KFImage(URL(string: product.gallery[0]))
                            .placeholder({ _ in
                                ProgressView()
                                    .progressViewStyle(CircularProgressViewStyle())
                            })
                            .resizable()
                            .scaledToFit()
                            .frame(width: 100)
                            .frame(maxHeight: 60)
                            .background {
                                Rectangle()
                                    .fill(.white)
                                    .frame(maxWidth: 120, maxHeight: 80)
                            }
                        
                        Spacer()
                    }
                    
                    if isFavoriteView {
                        VStack {
                            HStack {
                                Button {
                                    model.updateListFavorite(id: product.id, isFavorite: !model.checkFavorite(id: product.id))
                                } label: {
                                    Image(systemName: "heart.fill")
                                        .foregroundColor(
                                            Color(model.checkFavorite(id: product.id) ? NameConstant.Color.ColorF87265 : NameConstant.Color.ColorEEEEEE)
                                        )
                                }
                                Spacer()
                            }
                            .padding(.top, -10)
                            
                            Spacer()
                        }
                    }
                }
                
                HStack {
                    VStack(alignment: .leading) {
                        Text(Localization.bestSeller)
                            .font(Font.customWMd(size: 12))
                            .foregroundColor(
                                Color(NameConstant.Color.Color5B9EE1)
                            )
                        
                        Text(product.name)
                            .font(
                                Font.customWMd(size: 16)
                                    .weight(.medium)
                            )
                            .foregroundColor(
                                Color(NameConstant.Color.Color1A2530)
                            )
                            .lineLimit(2)
                            .multilineTextAlignment(.leading)

                        HStack(spacing: 0) {
                            Text(product.price.convertPrice())
                                .font(
                                    Font.customWMd(size: 14)
                                        .weight(.medium)
                                )
                                .foregroundColor(
                                    Color(NameConstant.Color.Color1A2530)
                                )

                            Spacer(minLength: 0)

                            HStack {
                                ForEach(product.color, id: \.self) { item in
                                    Circle()
                                        .stroke(Color.gray.opacity(0.5), lineWidth: 1)
                                        .frame(width: 16)
                                        .overlay {
                                            Circle()
                                                .fill(item.colorProduct())
                                                .frame(width: 15)
                                        }
                                }
                            }
                        }
                    }
                    
                    Spacer()
                }
            }
            .padding(.horizontal, 14)
            .padding(.top, 20)
            .padding(.bottom, 14)
        }
        .background(.white)
        .frame(maxWidth: 160, maxHeight: 240)
        .cornerRadius(16)
//        .onAppear {
//            isFavorite = model.listFavorite.contains(product.id)
//        }
//        .onAppear {
//            if isFavoriteView {
//                model.onAppear {
//                    if model.listFavorite.contains(product.id) {
//                        isFavorite = true
//                    } else {
//                        isFavorite = false
//                    }
//                }
//            }
//        }
    }
}
