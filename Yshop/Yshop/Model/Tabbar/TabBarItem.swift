//
//  TabBarItem.swift
//  Yshop
//
//  Created by GST.SYN.APL on 14/07/2023.
//

import Foundation

struct TabBarItem {
    let imageName: String
    let imageSelected: String
}

let tabItems = [
    TabBarItem(imageName: NameConstant.Image.tabbarHome, imageSelected: NameConstant.Image.tabbarHomeSelected),
    TabBarItem(imageName: NameConstant.Image.tabbarFavorite, imageSelected: NameConstant.Image.tabbarFavoriteSelected),
    TabBarItem(imageName: NameConstant.Image.tabbarCart, imageSelected: NameConstant.Image.tabbarCartSelected),
    TabBarItem(imageName: NameConstant.Image.tabbarPerson, imageSelected: NameConstant.Image.tabbarPersonSelected)
]

//let tabItems = [
//    TabBarItem(imageName: "home"),
//    TabBarItem(imageName: "heart"),
//    TabBarItem(imageName: "cart"),
//    TabBarItem(imageName: "user")
//]
