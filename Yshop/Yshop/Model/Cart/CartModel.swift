//
//  CartModel.swift
//  Yshop
//
//  Created by GST.SYN.APL on 03/08/2023.
//

import SwiftUI

struct CartModel: Identifiable, Codable, Hashable {
    let id: Int
    let name: String
//    let color: ColorType
    var count: Int
    let price: Double
    let image: String
    let size: Int
}

struct CheckoutModel: Identifiable {
    var id = UUID()
    var listProduct: [CartModel]
    var address: String
    var phone: String
    var email: String
}
