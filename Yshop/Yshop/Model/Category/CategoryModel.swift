//
//  CategoryModel.swift
//  Yshop
//
//  Created by GST.SYN.APL on 14/07/2023.
//

import Foundation

enum CategoryType: String {
    case nike
    case adidas
    case puma
    case underArmour
    case converse

    func getImageName() -> String {
        switch self {
        case .nike:
            return NameConstant.Image.nikeIcon
        case .adidas:
            return NameConstant.Image.adidasIcon
        case .puma:
            return NameConstant.Image.pumaIcon
        case.underArmour:
            return NameConstant.Image.underArmourIcon
        case .converse:
            return NameConstant.Image.converseIcon
        }
    }

    func getDescription() -> String {
        switch self {
        case .nike:
            return Localization.descriptionNike
        case .adidas:
            return Localization.descriptionAdidas
        case .puma:
            return Localization.descriptionPuma
        case .underArmour:
            return Localization.descriptionUnderAmour
        case .converse:
            return Localization.descriptionConverse
        }
    }
}

let listCategory: [CategoryType] = [.nike, .puma, .underArmour, .adidas, .converse]

class CategoryModel: ObservableObject {
    @Published var selectedIndex: Int = 0
}
