//
//  ProductModel.swift
//  Yshop
//
//  Created by GST.SYN.APL on 14/07/2023.
//

import SwiftUI

enum ColorType: String {
    case orange
    case gray
    case red
    case black
    case white
    case blue
    case green
    case yellow

    func colorProduct() -> Color {
        switch self {
        case .orange:
            return Color.orange
        case .gray:
            return Color.gray
        case .red:
            return Color.red
        case .black:
            return Color.black
        case .white:
            return Color.white
        case .blue:
            return Color.blue
        case .green:
            return Color.green
        case .yellow:
            return Color.yellow
        }
    }
}

struct ProductElement: Identifiable {
    let id: Int
    let name: String
    let gallery: [String]
    let price: Double
    var count: Int
    let color: [ColorType]
    let category: CategoryType
}


class ProductInApp: ObservableObject {
    @Published var productList: [ProductElement] = []
}
