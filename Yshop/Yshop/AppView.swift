//
//  AppView.swift
//  Yshop
//
//  Created by GST.SYN.APL on 14/07/2023.
//

import SwiftUI

struct AppView: View {
    @ObservedObject private var model = AppViewModel()
    @State private var selectedIndex = 0
    @EnvironmentObject var productList: ProductInApp

    var body: some View {
        NavigationView {
            ZStack {
                GeometryReader { reader in
                    Color(NameConstant.Color.ColorF8F9FA)
                        .frame(height: reader.safeAreaInsets.top, alignment: .top)
                        .ignoresSafeArea()
                }

                VStack {
                    if selectedIndex == 0 {
                        HomeView()
                    } else if selectedIndex == 1 {
                        FavoriteView()
                    } else if selectedIndex == 2 {
                        CartView()
                    } else {
                        UserView()
                    }
                    TabbarView(selectedIndex: $selectedIndex)
                        .frame(height: 60)
                }
                .overlay(content: {
                    if !model.show {
                        VStack {
                            ProgressView(Localization.loading)
                                .progressViewStyle(.circular)
                        }
                        .frame(width: 200, height: 100)
                        .background(
                            Rectangle()
                                .fill(.white)
                                .frame(width: 200, height: 100)
                                .cornerRadius(16)
                        )
                    }
                })
                .background(
                    Color(NameConstant.Color.ColorF8F9FA)
                )
                .edgesIgnoringSafeArea(.bottom)
            }
        }
        .alert(Localization.getDataError, isPresented: $model.callError, actions: {
            Text(Localization.ok)
        })
        .onAppear {
            model.getData {
                self.productList.productList = self.model.pr
                model.show = true
            }
        }
    }
}

struct AppView_Previews: PreviewProvider {
    static var previews: some View {
        AppView()
            .environmentObject(ProductInApp())
    }
}
