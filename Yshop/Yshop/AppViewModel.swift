//
//  HomeViewModel.swift
//  Yshop
//
//  Created by GST.SYN.APL on 01/08/2023.
//

import SwiftUI
import Firebase
import FirebaseDatabase

class AppViewModel: ObservableObject {
    private var ref = Database.database().reference()
    @Published var pr: [ProductElement] = []
    @Published var callError: Bool = false
    @Published var errorString: String = ""
    @Published var show: Bool = false

    func getData(completion: @escaping ()-> Void) {
        ref.child("product").getData(completion:  { error, snapshot in
            guard error == nil else {
                self.show = true
                self.errorString = error?.localizedDescription ?? ""
                self.callError = true
                return
            }
            let userName = snapshot?.value as? [NSDictionary]
            guard let userName = userName else {
                return
            }
            for product in userName {
                let id: Int = product.value(forKey: "id") as? Int ?? 0
                let name: String = product.value(forKey: "name") as? String ?? ""
                let gallery: String = product.value(forKey: "gallery") as? String  ?? ""
                let color: String = product.value(forKey: "color") as? String  ?? ""
                let category: String = product.value(forKey: "category") as? String ?? ""
                let price: Double = product.value(forKey: "price") as? Double ?? 0
                let count: Int = product.value(forKey: "count") as? Int ?? 0

                let colorList = color.components(separatedBy: ", ")
                let galleryList = gallery.components(separatedBy: ", ")

                let newPro = ProductElement(id: id,
                                            name: name,
                                            gallery: galleryList,
                                            price: price,
                                            count: count,
                                            color: self.convertColor(colorList: colorList),
                                            category: CategoryType(rawValue: category) ?? .nike)
                self.pr.append(newPro)
            }
            completion()
        })
    }

    private func convertColor(colorList: [String]) -> [ColorType] {
        var listColor: [ColorType] = []
        for item in colorList {
            listColor.append(ColorType(rawValue: item) ?? .black)
        }
        return listColor
    }
}
