//
//  RealmData.swift
//  Yshop
//
//  Created by GST.SYN.APL on 10/08/2023.
//

import Foundation
import RealmSwift

class CartRealmData: Object, Identifiable {
    @Persisted(primaryKey: true) var id: Int
    @Persisted var name: String
    @Persisted var image: String
    @Persisted var count: Int
    @Persisted var price: Double
    @Persisted var size: Int
}

class FavoriteRealmData: Object, Identifiable {
    @Persisted var id: Int
}
