//
//  File.swift
//  Yshop
//
//  Created by GST.SYN.APL on 21/07/2023.
//

import SwiftUI

extension Font {
    static func customBold(size: CGFloat) -> Font {
        Font.custom(NameConstant.Font.AirbnbCereal_W_Bd, size: size)
    }

    static func customNormal(size: CGFloat) -> Font {
        Font.custom(NameConstant.Font.AirbnbCereal_W_Bk, size: size)
    }

    static func customXBd(size: CGFloat) -> Font {
        Font.custom(NameConstant.Font.AirbnbCereal_W_XBd, size: size)
    }

    static func customWMd(size: CGFloat) -> Font {
        Font.custom(NameConstant.Font.AirbnbCereal_W_Md, size: size)
    }
}
