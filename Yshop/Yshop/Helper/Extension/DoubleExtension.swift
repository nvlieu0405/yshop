//
//  DoubleExtension.swift
//  Yshop
//
//  Created by GST.SYN.APL on 13/08/2023.
//

import Foundation

extension Double {
    func convertPrice() -> String {
        let priceString = String(format: "%.2f", self)
        return "$\(priceString)"
    }
}
