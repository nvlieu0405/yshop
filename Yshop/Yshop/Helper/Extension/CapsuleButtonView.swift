//
//  CapsuleButtonView.swift
//  Yshop
//
//  Created by GST.SYN.APL on 21/07/2023.
//

import SwiftUI

struct CapsuleButtonView: View {
    var text: String
    var body: some View {
        ZStack {
            Text(text)
                .font(
                    Font.customWMd(size: 18)
                        .weight(.medium)
                )
                .foregroundColor(.white)
                .padding(.horizontal, 32)
                .padding(.vertical, 16)
        }
        .background(
            Capsule()
                .fill(.blue)
        )
    }
}

struct CapsuleButtonView_Previews: PreviewProvider {
    static var previews: some View {
        CapsuleButtonView(text: "test")
    }
}
