//
//  StringExtension.swift
//  Yshop
//
//  Created by GST.SYN.APL on 13/08/2023.
//

import Foundation

extension String {
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: self)
    }

    func isValidPhoneNumber() -> Bool {
        let numericCharacterSet = CharacterSet.decimalDigits
        let phoneNumberDigits = self.trimmingCharacters(in: numericCharacterSet.inverted)
        return phoneNumberDigits.count == 10
    }

    func isPasswordValid() -> Bool{
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[a-z])(?=.*[$@$#!%*?&])[A-Za-z\\d$@$#!%*?&]{8,}")
        return passwordTest.evaluate(with: self)
    }
}
