//
//  Constant.swift
//  Yshop
//
//  Created by GST.SYN.APL on 14/07/2023.
//

import Foundation

struct NameConstant {
    struct Image {
        static let nikeIcon: String = "nike-icon"
        static let adidasIcon: String = "adidas-icon"
        static let pumaIcon: String = "puma-icon"
        static let jordanIcon: String = "jordan-icon"
        static let converseIcon: String = "converse-icon"
        static let underArmourIcon: String = "under-armour-icon"

        static let searchIcon: String = "search-icon"

        static let tabbarHome: String = "home-black"
        static let tabbarHomeSelected: String = "home-blue"
        static let tabbarFavorite: String = "heart-black"
        static let tabbarFavoriteSelected: String = "heart-blue"
        static let tabbarCart: String = "bag-black"
        static let tabbarCartSelected: String = "bag-blue"
        static let tabbarPerson: String = "user-black"
        static let tabbarPersonSelected: String = "user-blue"

        static let shoesDefault: String = "shoes-default"
        static let addIcon: String = "add-icon"
        static let menuIcon: String = "menu-icon"
        static let locationIcon: String = "location-icon"
        static let cartIcon: String = "cart-icon"

        static let google: String = "Google"
        static let splash: String = "Splash"
        static let imageSplash1: String = "image-Splash-1"
        static let imageSplash2: String = "image-Splash-2"
        static let imageSplash3: String = "image-Splash-3"
        static let firstStep: String = "firstStep"
        static let secondStep: String = "secondStep"
        static let thirdStep: String = "thirdStep"

        static let bottomDetail: String = "bottomDetail"
        static let plusIcon: String = "plus-icon"
        static let minusIcon: String = "minus-icon"
        static let deleteIcon: String = "delete-icon"

        static let editIcon: String = "edit-icon"
        static let editIconBlue: String = "edit-icon-blue"
        static let emailIcon: String = "email-icon"
        static let callIcon: String = "call-icon"
        static let successImage: String = "success-image"
    }

    struct Color {
        static let ColorEEEEEE: String = "ColorEEEEEE"
        static let ColorF8F9FA: String = "ColorF8F9FA"
        static let Color707B81: String = "Color707B81"
        static let Color1A2530: String = "Color1A2530"
        static let Color5B9EE1: String = "Color5B9EE1"
        static let ColorA4CDF6: String = "ColorA4CDF6"
        static let Color101817: String = "Color101817"
        static let ColorF87265: String = "ColorF87265"
    }

    struct Font {
        static let AirbnbCereal_W_Bd = "AirbnbCereal_W_Bd"
        static let AirbnbCereal_W_Bk = "AirbnbCereal_W_Bk"
        static let AirbnbCereal_W_Blk = "AirbnbCereal_W_Blk"
        static let AirbnbCereal_W_Lt = "AirbnbCereal_W_Lt"
        static let AirbnbCereal_W_Md = "AirbnbCereal_W_Md"
        static let AirbnbCereal_W_XBd = "AirbnbCereal_W_XBd"
    }
}
