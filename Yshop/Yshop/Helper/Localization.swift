//
//  Localization.swift
//  Yshop
//
//  Created by GST.SYN.APL on 17/07/2023.
//

import Foundation

struct Localization {
    static let storeLocation = "Store location"
    static let locationDefault = "Mondolibug, Sylhet"
    static let popularShoes = "Popular Shoes"
    static let searchText = "Looking for shoes"
    static let cancel = "Cancel"
    static let bestSeller = "Best Seller"
    static let menShoes = "Men’s Shoes"
    static let gallery = "Gallery"
    static let size = "Size"
    static let price = "Price"
    static let addToCart = "Add To Cart"
    static let error = "Error"
    static let pleaseChooseSize = "Please choose size"
    static let ok = "OK"
    static let success = "Success"
    static let successContent = "The product has been added to your cart"
    static let nike = "nike"

    static let title1 = "Start Journey With Nike"
    static let description1 = "Smart, Gorgeous & Fashionable Collection"
    static let getStarted = "Get started"

    static let title2 = "Follow Latest Style Shoes"
    static let description2 = "There Are Many Beautiful And Attractive Plants To Your Room"
    static let next = "Next"

    static let title3 = "Summer Shoes Nike 2023"
    static let description3 = "Amet Minim Lit Nodeseru Saku Nandu sit Alique Dolor"

    static let helloAgain = "Hello Again!"
    static let createAccount = "Create Account"
    static let welcomeBack = "Welcome Back You’ve Been Missed!"
    static let letCreate = "Let’s Create Account Together"
    static let yourName = "Your name"
    static let emailAddress = "Email Address"
    static let password = "Password"
    static let recoveryPassword = "Recovery Password"
    static let signIn = "Sign In"
    static let signInGG = "Sign in with google"
    static let dontHaveAccount = "Don’t have an account?"
    static let alreadyAccount = "Already have an account?"
    static let signUpFree = "Sign Up for free"

    static let loading = "Loading..."
    static let passwordInvalid = "Passwords invalid."
    static let resetPass = "Please Enter Your Email Address To Recieve a Verification Code"
    static let continueText = "Continue"
    static let profile = "Profile"
    static let fullName = "Full Name"
    static let signOut = "Sign Out"

    static let areYouDelete = "Are you sure delete?"
    static let no = "No"
    static let yes = "Yes"
    static let paymenSuccess = "Your Payment Is Successful"
    static let backToShop = "Back To Shopping"

    static let myCart = "My Cart"
    static let subTotal = "Subtotal"
    static let shopping = "Shopping"
    static let totalCost = "Total Cost"
    static let checkOut = "Checkout"
    static let notification = "Notification"
    static let pleaseLogin = "Please login to checkout"
    static let contactInfor = "Contact Information"
    static let email = "Email"
    static let phone = "Phone"
    static let address = "Address"
    static let payment = "Payment"

    static let favourite = "Favourite"

    static let seeAll = "See all"
    static let bestChoice = "Best Choice"

    static let descriptionNike = "Nike shoes are durable. Shoes are made with materials that are durable. Some material used in this shoes has specific uses on depending of the kind of sport that the shoe is going to be designed. Durability in all sport shoes are an important criteria to apply. The more durable a shoe is, the better acceptance to the people with economic problems will be."

    static let descriptionAdidas = "Adidas shoes and clothes are some of the best in the industry today. It has been a very long time since Adidas started producing different kinds of shoes such as sneakers and running shoes and many others."

    static let descriptionPuma = "If you are in search of the most popular and versatile running shoes then you have many options to consider but the best option for you is the shoes offered by Puma Australia. This is a renowned shoe brand that has been selling a large number of shoes to men and women across the world."

    static let descriptionUnderAmour = "In only a decade, Under Armour has become a well-recognized name. More importantly, it is associated with quality and innovation, which means that it is easier to charge higher prices."

    static let descriptionConverse = "Converse is one of the leading brands in the lifestyle and retail sector. Converse SWOT analysis evaluates the brand by its strengths & weaknesses which are the internal factors along with opportunities & threats which are the external factors."


    static let isFirstRun = "isFirstRun"
    static let getDataError = "Get Data Error"
    static let errorInfor = "Please complete and correct information"
    static let pleaseCheckEmail = "Please check your email"
    static let yourRequestSuccess = "Your request success"
    static let pleaseEnterEmail = "Please enter email correct"
    static let updateProfileSuccess = "Update profile success"

}
