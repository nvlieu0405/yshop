//
//  TextFieldModifier.swift
//  Yshop
//
//  Created by GST.SYN.APL on 19/07/2023.
//

import SwiftUI

struct TextFieldModifier: ViewModifier {
    var isDisable: Bool = false
    func body(content: Content) -> some View {
        content
            .frame(height: 48)
            .padding(.leading, 14)
            .background(isDisable ? .gray.opacity(0.4) : .white)
            .foregroundColor(
                isDisable ? .gray : Color(NameConstant.Color.Color1A2530)
            )
            .font(
                Font.custom(NameConstant.Font.AirbnbCereal_W_Bk, size: 16)
                    .weight(.medium)
            )
            .cornerRadius(50)
    }
}
