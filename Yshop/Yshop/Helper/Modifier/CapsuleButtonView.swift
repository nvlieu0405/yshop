//
//  CapsuleButtonView.swift
//  Yshop
//
//  Created by GST.SYN.APL on 21/07/2023.
//

import SwiftUI

struct CapsuleButtonView: ViewModifier, View {
    var text: String
    func body(content: Content) -> some View {
        ZStack {
            Text(text)
                .font(
                    Font.customNormal(size: 18)
                        .weight(.medium)
                )
                .foregroundColor(.white)
                .padding(.horizontal, 32)
                .padding(.vertical, 16)
        }
        .background(
            Capsule()
                .fill(.blue)
        )
    }
}
