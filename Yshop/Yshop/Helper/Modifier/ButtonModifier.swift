//
//  ButtonModifier.swift
//  Yshop
//
//  Created by GST.SYN.APL on 19/07/2023.
//

import SwiftUI

extension Image {
    func myImageModifier() -> some View {
        self
            .resizable()
            .scaledToFit()
            .frame(width: 14, height: 14, alignment: .center)
            .foregroundColor(Color.black)
            .background(
                Rectangle()
                    .fill(.white)
                    .frame(width: 44, height: 44)
                    .cornerRadius(40)
            )
   }
}
